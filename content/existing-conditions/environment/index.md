---
title: "Environment"
date: 2018-09-18T13:50:42-06:00
draft: false
weight: 30
bannerHeading: Environment
bannerText: >
  It is critical to consider the natural environment when accounting for the
  short- and long-term impacts of transportation decisions.
---

It is critical to consider the natural environment when accounting for the
short- and long-term impacts of transportation decisions. In connection with new
approaches to how we maintain and enhance the livability of our region, current
Federal and State transportation legislation reconfirm the need to enhance the
performance of transportation systems while protecting and enhancing the natural
environment as one of its primary goals. Managing environmental resources as a
group of strategic assets that are crucial to municipal goals, important to
ecosystem health, and beneficial to the region is key to successful regional
management. Key environmental assets include but are not limited to:

 - Air  
 - Water
 - Biodiversity  
 - Natural Areas

The natural environment provides the region with several ecosystem services
which are fundamental to urban livability. In considering environmental
resources, these benefits may be managed and increased by planning
transportation networks in a way that preserves, unifies, and invests in these
natural systems.

## Air ##

Climate change cause by human activities is a growing global threat. The
National Oceanic and Atmospheric Administration (NOAA) projects ongoing
increases in Illinois temperatures and extreme precipitation events depending on
future levels of greenhouse gas emissions[^1]. These climate changes and their
impact on many industries and different aspects of life are important to
consider when planning for the year 2045.

At the national level, the transportation sector accounted for the largest
portion of total U.S. greenhouse gas emissions in 2017. [The most recent data
for Illinois](https://statesummaries.ncics.org/chapter/il/) shows a similar
pattern: in 2016 the transportation sector accounted for the largest share of
carbon dioxide emissions. While all types of vehicles contribute to
transportation sector emissions, the passenger cars and light-duty trucks most
area residents rely on every day are responsible for the majority of emissions
within the transportation sector.


<rpc-chart
  url="epa-emissions-overall-2017.csv"
  chart-title="U.S. Greenhouse Gas Emissions by Sector, 2017"
  type="doughnut"
  source=" United States Environmental Protection Agency. Accessed 18 July 2019."
  source-url="https://www.epa.gov/greenvehicles/fast-facts-transportation-greenhouse-gas-emissions">
</rpc-chart>  

<rpc-chart
  url="epa-emissions-trans-2017.csv"
  chart-title="U.S. Transportation Greenhouse Gas Emissions by Source, 2017"
  type="doughnut"
  source=" United States Environmental Protection Agency. Accessed 18 July 2019."
  source-url="https://www.epa.gov/greenvehicles/fast-facts-transportation-greenhouse-gas-emissions">
</rpc-chart>  


Climate change can impact air quality, and air quality can also impact climate
change. For instance, ozone in the atmosphere warms the climate, while different
components of particulate matter (PM) can have either warming or cooling effects
on the climate[^3].

The Champaign-Urbana Urbanized Area maintains an overall “attainment status” for
locally measured air pollutants, which means that air quality in the Champaign-Urbana Metropolitan area meets federal air quality standards. The 2017 Air Quality Index for the Metropolitan Area shows 78.4 percent of days were measured
as having Good air quality, 21.4 percent were measured as “Moderate”, and 0.3
percent were considered “Unhealthy for Sensitive Groups.” The Air Quality Index
represents a summary assessments for six different air pollutants carbon
monoxide, nitrogen dioxide, ozone, sulfur dioxide, and two
forms of particulate matter (PM2.5 and PM10).

<rpc-chart
  url="il-epa-airquality2017.csv"
  chart-title="Champaign-Urbana Metropolitan Area Air Quality, 2017"
  type="doughnut"
  source=" Illinois Environmental Protection Agency. Annual Air Quality Report Final. Accessed 9 September 2019."
  source-url="https://www2.illinois.gov/epa/topics/air-quality/air-quality-reports/Pages/default.aspx">
</rpc-chart>  

Together with the U.S. Environmental Protection Agency (EPA), the Illinois EPA
monitors the concentrations of the six airborne pollutants represented in the
Air Quality Index as well as a seventh, lead. Four of these pollutants are
measured in Champaign County. The most comprehensive monitoring site in the
County is in Bondville, monitoring carbon monoxide, one form of particulate
matter (PM2.5), ozone, and sulfur dioxide. Due to its distance from the
urbanized area and the prevailing winds from the south-southwest, the Bondville
site is categorized as a rural National Core Pollutant Monitoring site (NCore),
rather than an urban monitoring site. Champaign County has two additional air
monitoring sites, one in the City of Champaign that monitors one form of
particulate matter (PM2.5) and one in Thomasboro that monitors ozone. Lead,
nitrogen dioxide, and particulate matter (PM10) are not monitored in Champaign
County.

The following table provides a summary of each pollutant measured by the
Illinois EPA, alongside the primary national standards and local monitoring
stations. “Primary Standards” refer to air quality levels required to protect
public health with an adequate margin of safety.

<rpc-table
url="air-naaqs-2017.csv"
table-title="National and Illinois Ambient Air Quality, Primary Standards, 2017"
source=" Illinois Environmental Protection Agency, 2017 Air Quality Report. Accessed 2 February 2019"
source-url="https://www2.illinois.gov/epa/Pages/default.aspx">
</rpc-table>


### Carbon Monoxide ###

Carbon monoxide is produced when fuel or other carbon-based materials are
burned. The main source of carbon monoxide emissions is motor vehicles. The U.S.
EPA sets emissions standards for new motor vehicles while the State of Illinois
focuses on overseeing transportation plans for congested urban areas as a
strategy for reducing carbon monoxide in the air.

The 1-hour and 8-hour carbon monoxide standards are set at 35 parts per million
(ppm) and 9 ppm, respectively, and are not to be exceeded more than once per
year. The Bondville monitoring station did not record any exceedances of the
1-hour or 8-hour standards between 2013 and 2018.

<rpc-chart
url="1hr-carbon_monoxide2011-2017.csv"
type="bar"
switch="true"
colors="indigo,blue"
legend-position="top"
legend="true"
y-label="Carbon Monoxide 1-hour Design Values (ppb)"
y-max="40"
tooltip-intersect="true"
source=" Illinois Environmental Protection Agency, 2011-2017 Air Quality Reports, 2 February 2019"
source-url="https://www2.illinois.gov/epa/topics/air-quality/air-quality-reports/Pages/default.aspx"
description="Note: The original NAAQS was established in 1971.  EPA reviewed both the 1-hour and 8-hour CO standards and decided to retain these standards in 2011."
chart-title="Carbon Monoxide 1-hour Standard: Champaign County Compliance, 2013-2017">
<rpc-dataset
type="line"
fill="true"
border-color="#470b11"
border-width="1"
point-radius="0"
order="3"
label="2011 NAAQS Standard"
data="35.0,35.0,35.0,35.0,35.0,35.0">
</rpc-dataset></rpc-chart>

<rpc-chart
url="8hr-carbon-monoxide2011-2017.csv"
type="bar"
switch="true"
colors="indigo,blue"
legend-position="top"
legend="true"
y-label="Carbon Monoxide 8-hour Design Values (ppb)"
y-max="10"
tooltip-intersect="true"
description="Note: The original CO NAAQS was established in 1971.  EPA reviewed both the 1-hour and 8-hour CO standards and decided to retain these standards in 2011."
source=" Illinois Environmental Protection Agency, 2011-2017 Air Quality Reports, 2 February 2019"
source-url="https://www2.illinois.gov/epa/topics/air-quality/air-quality-reports/Pages/default.aspx"
chart-title="Carbon Monoxide 8-hour Standard: Champaign County Compliance, 2013-2017">
<rpc-dataset
type="line"
fill="true"
border-color="#470b11"
border-width="1"
point-radius="0"
order="3"
label="2011 NAAQS Standard"
data="9.0,9.0,9.0,9.0,9.0,9.0">
</rpc-dataset></rpc-chart>


### Nitrogen Dioxide ###

Currently, there are no nitrogen dioxide monitoring sites in Champaign County.
According to the Illinois EPA, there were no violations of the nitrogen dioxide
standards recorded in Illinois between 2008 and 2017.

### Ozone ###
Ground level ozone is a secondary air pollutant produced by chemical reactions
in the environment with man-made factors such as vehicle exhaust, gasoline
vapors, and industrial emissions. Two main pollutants, nitrogen oxides and
volatile organic compounds, contribute to ground level ozone pollution. Heat
serves as a catalyst to accelerate ozone-forming reactions.

The 8-hour ozone standard is set at 70 parts per billion (ppb), measured by the
three-year average of the annual fourth-highest daily eight-hour concentration
maximum, effectively permitting three exceedances per year. Ozone monitoring
stations in Champaign County recorded one 8-hour measure of ozone above 70 ppb
in 2017, but the 3-year average of the fourth-highest eight-hour measures
remained below the national standard.

<rpc-chart
url="air-ozone2011-2017.csv"
type="bar"
switch="true"
colors="indigo,blue"
rows="1,2,3,4,5,6"
legend-position="top"
legend="true"
y-label="Ozone in Parts per Billion (PPB)"
y-min="60"
tooltip-intersect="true"
source=" Illinois Environmental Protection Agency, 2011-2017 Air Quality Reports, 2 February 2019"
source-url="https://www2.illinois.gov/epa/topics/air-quality/air-quality-reports/Pages/default.aspx"
description="Note: The standard for ozone changed from 75ppb to 70ppb in 2015."
chart-title="Ozone Standard: Champaign County Compliance, 2011-2017">
<rpc-dataset
type="line"
fill="false"
border-color="#470b11"
border-width="1"
point-radius="0"
order="3"
label="2008 NAAQS Standard (previous)"
data="75.0,75.0,75.0,75.0,75.0,75.0">
</rpc-dataset><rpc-dataset
type="line"
fill="false"
border-color="red"
border-width="1"
point-radius="0"
order="4"
label="2015 NAAQS Standard (current)"
data="70.0,70.0,70.0,70.0,70.0,70.0">
</rpc-dataset></rpc-chart>

### Sulfur Dioxide ###

A large portion of sulfur dioxide pollution results from burning fossil fuels
containing sulfur compounds. The use of low- sulfur content fuels and the use of
chemical sulfur removal systems have been effective at reducing sulfur dioxide
pollution levels.

The Bondville monitoring station shows local levels of sulfur dioxide to be
considerably below the national standard and steadily declining between 2013 and
2017.

<rpc-chart
url="sulfur-dioxide-2011-2017.csv"
type="bar"
switch="true"
colors="indigo,blue"
rows="1,2,3,4,5,6"
legend-position="top"
legend="true"
y-label="Sulfur Dioxide 1-hour Design Values (ppb)"
y-max="80"
tooltip-intersect="true"
source=" Illinois Environmental Protection Agency, 2011-2017 Air Quality Reports, Accessed 2 February 2019"
source-url="https://www2.illinois.gov/epa/topics/air-quality/air-quality-reports/Pages/default.aspx"
description="Note: On June 2, 2010, the EPA revised the SO2 standard, establishing a 1-hour primary standard of 75 ppb."
chart-title="Sulfur Dioxide Standard: Champaign County Compliance, 2011-2017">
<rpc-dataset
type="line"
fill="true"
border-color="#470b11"
border-width="1"
point-radius="0"
order="3"
label="2010 NAAQS Standard"
data="75.0,75.0,75.0,75.0,75.0,75.0">
</rpc-dataset></rpc-chart>

### Lead ###

Historically, motor fuel was the primary source of lead air pollution. Through
effective air quality control measures, leaded gasoline was phased-out of the
market beginning in the mid-1970’s. The result was approximately a 90 percent
reduction in lead air pollution. This demonstrates that changes in
transportation emissions regulations can have a significant impact on the
environment.

Currently, there are no lead monitoring sites in Champaign County. According to
the Illinois EPA, there were no violations of the lead standard recorded in
Illinois between 2008 and 2017.

### Particulate Matter ###

Particulate matter is a type of airborne pollutant consisting of small liquid or
solid particles in the atmosphere. Unlike other pollutant categories,
particulate matter does not designate a specific chemical composition. Major
sources of PM2.5 result from burning or combustion activities such as the
burning of fossil fuels, smelting, and forest fires. In the context of Champaign
County, it is important to note that ammonia from agricultural sources such as
fertilizer and animal feed operations contributes to the formation of sulfurous
and nitrogenous particulate matter that exists in the atmosphere[^1].

Within the last decade, Champaign County monitoring stations have recorded no
exceedances of the annual or 24-hour standards for PM2.5. In addition, the
three-year averages of PM2.5 in the county have been steadily decreasing since
2011. PM10 is not currently monitored in Champaign County. According to the
Illinois EPA, there were no violations of the PM10 standards recorded in
Illinois between 2008 and 2017.

<rpc-chart
url="air-pm24hr2011-2017.csv"
type="bar"
y-label="PM2.5 in Micrograms per Cubic Meter (mcg/m3)"
y-min="10"
y-max="40"
switch="true"
colors="indigo,blue"
rows="1,2,3,4,5,6"
columns="1,3,4"
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" Illinois Environmental Protection Agency, 2011-2017 Air Quality Reports, 2 February 2019"
source-url="https://www2.illinois.gov/epa/topics/air-quality/air-quality-reports/Pages/default.aspx"
chart-title="Particulate Matter Standard: Champaign County Compliance, 2011-2017">
<rpc-dataset
type="line"
fill="false"
border-color="#470b11"
border-width="1"
point-radius="0"
order="4"
label="NAAQS 24-Hour Standard"
data="35.0,35.0,35.0,35.0,35.0">
</rpc-dataset></rpc-chart>


## Water ##

The Illinois EPA evaluates water quality compliance based on support, or
attainment, of each designated use. Water quality information is based on the
Illinois EPA's biannual Illinois Integrated Water Quality Report and Section
303(d) Lists. These documents offer information related to the condition of
surface water as required by the federal Clean Water Act. The assessments
contained in these reports primarily come from biological, water, physical
habitat, and fish-tissue data collected through monitoring programs across the
state. Due to limited resources, the Illinois EPA typically assesses approximately 15
percent of Illinois stream[^2] miles for at least one designated use during
every reporting cycle. Within the Metropolitan Planning Area, aquatic life, fish
consumption, and primary contact are the most frequently observed designated
uses. A stream is classified as "impaired" if it cannot support any one of its
designated uses. Impaired streams are placed on the 303(d) list, starting the
recovery process. Streams on the 303(d) list require additional data collection
and a priority ranking for establishing a total maximum daily load (TMDL).

<iframe src="https://maps.ccrpc.org/lrtp2045-ecr-water-quality/"
  width="100%" height="600" allowfullscreen="true">
  Map showing the 2018 water quality assessments of streams in the MPA.
</iframe>

Water quality in the region has had and continues to have mixed results. In the
Illinois EPA's 2018 Integrated Water Quality Report, seven streams received an
impaired classification, while five fully supported their designated uses.
Limited assessments indicate no streams in the MPA support fish consumption or
primary contact. Boneyard Creek, both segments of Hackett Branch, and Saline
Branch segment IL_BPJC-08 have consistently failed to support aquatic life since
2008. Saline Branch segment IL_BPJC-06 was previously on the 303(d) List, but
has maintained support of its designated uses since 2010. Two streams in the
region, Union Drainage Ditch and Phinney Branch, have not been assessed for many
years.

  <rpc-table url="water-quality-20182.csv"
    table-title="Water Segments Supporting Aquatic Life in MPA, 2010-2018"
    source=" Illinois Environmental Protection Agency, 2018 Illinois Integrated Water Quality Report and Section 202(d) List. Accessed 7 February 2019"
    source-url="https://www2.illinois.gov/epa/topics/water-quality/"
    description="*Embarras River segment IL_BE-25 was previously listed as IL_BE-14. In the 2010 Illinois Integrated Water Quality Report, this segment was split. Segment IL_BE-25 is the portion currently in the MPA"*
    text-alignment="l"></rpc-table>

## Biodiversity ##

Natural areas like grasslands, woodlands, and wetlands provide optimal
conditions for many native plant and animal species. Native species depending on
these specific habitats become vulnerable as these habitats are lost to
development, pollution, and a changing climate. In July 2018, there were a total
of [33 threatened and endangered species in Champaign
County](https://www.dnr.illinois.gov/ESPB/Pages/default.aspx) identified by the
Illinois Department of Natural Resources (IDNR). This is an increase from 22
threatened and endangered species in 2013 and 17 species in 2009. Plant species
experienced the most significant change in threatened and endangered listings,
more than tripling in number since 2013.

<rpc-chart
  url="eco-speciesthreatened-delta.csv"
  chart-title="Threatened and Endangered Species: Champaign County, 2009-2018"
  type="bar"
  switch="true"
  stacked="true"
  colors="indigo,blue"
  y-label="Number of Threatened and Endangered Species"
  switch="true"
  legend-position="top"
  legend="true"
  tooltip-intersect="true"
  source=" Illinois Department of Natural Resources, 2009-2018 Illinois Natural Heritage Database, 5 December 2018">
</rpc-chart>

It is worthwhile to note that some species classified as threatened or
endangered in Champaign County may be more prevalent in other regions of the
country or world.  Global classifications from the International Union for
Conservation of Nature Red List of Threatened Species (IUCN Red List) assist in
differentiating which species are of most concern both globally and locally.
Established in 1964, the [IUCN Red
List](https://www.iucnredlist.org/about/background-history) is one of the most
comprehensive information sources on global conservation. In Champaign
County, two animal species were both globally and locally endangered: the Rusty
Patched Bumble Bee and Blanding’s Turtle.

<rpc-table url="eco-specieslist-2018-plants.csv"
  table-title="Threatened and Endangered Plant Species: Champaign County, 2013 and 2018"
  columns="-2"
  source=" Illinois Department of Natural Resources, 2013-2018 Natural Heritage Database, 5 December 2018 "
  source-url="https://www.dnr.illinois.gov"
  text-alignment="l"></rpc-table>

<rpc-table url="eco-specieslist-2018-animals.csv"
  table-title="Threatened and Endangered Animal Species: Champaign County, 2013 and 2018"
  source=" Illinois Department of Natural Resources, 2013-2018 Natural Heritage Database, 5 December 2018"
  source-url="https://www.dnr.illinois.gov"
  text-alignment="l"></rpc-table>

## Natural Areas ##

The Illinois Natural Heritage Database is an indispensable resource for
identifying significant natural areas. Maintained by the Illinois Department of
Natural Resources, this database contains detailed information on wildlife and
natural areas including, threatened and endangered species habitat locations,
the Illinois Natural Areas Inventory (INAI), and lands with Illinois Nature
Preserves Commission (INPC) protection. The INAI designates and classifies
high-quality land, whereas the INPC establishes protection for high-quality
land. The [INAI guides INPC
determination](https://www.dnr.illinois.gov/INPC/Pages/VIsion-and-Benefits.aspx),
however not all recognized natural areas are also protected. Eleven natural
areas within the metropolitan planning area are classified in the Illinois
Natural Heritage Database: seven INAI sites and four INPC sites. Seven of the
natural areas are within the greater Mahomet area, two natural areas are
northeast of Urbana, and two natural areas are directly south of Urbana.

In 2018, land cover within the metropolitan planning area consisted of
approximately 76,000 acres of agricultural/grassland, 36,400 acres of urban
landscape, 4,000 acres of wooded areas, and 1,800 of wetlands and other water
bodies[^3]<sup>,</sup>[^4]<sup>,</sup>[^5]. Wetlands, in particular, greatly
assist in reducing the effects of regional flooding during times of heavy
precipitation, in addition to providing habitat for specific types of vegetation
and animal species not found in other environments. Since the previous
long-range transportation plan, wetlands and water bodies have not changed
significantly. The wetland and water total acreage has increased, but much of
this change is attributed to changes in the MPA boundary.

<iframe src="https://maps.ccrpc.org/lrtp2045-ecr-natural-areas/"
  width="100%" height="600" allowfullscreen="true">
  Map showing the 2018 natural areas within the MPA, including the
  IDNR Natural Heritage Database, wooded areas, and water bodies and wetlands.
</iframe>


[^1]: The Particle Pollution Report: Current Understanding of Air Quality and Emissions through 2003. Neil Frank. 2006. The Chemical Composition of PM2.5 to support PM Implementation. AQAG/AQAD USEPA. Pomporn Chantara. 2012. PM10 and Its Chemical Composition. Chiang Mai University.
[^2]: In the Illinois water quality reports, streams encompass a variety of water bodies including rivers, creeks, sloughs, and branches.
[^3]: United States Fish and Wildlife Service, [National Wetlands Inventory], Accessed 12/7/2018
[National Wetlands Inventory]: https://www.fws.gov/wetlands/data/mapper.html
[^4]: National Agricultural Statistics Service, [2018 CropScape - Cropland Data Layer], Accessed 2/26/2019
[2018 CropScape - Cropland Data Layer]: https://nassgeodata.gmu.edu/CropScape/
[^5]: Data has been collected from multiple sources. As a result, MPA acreage is an approximation and may contain some overlap between categories.
