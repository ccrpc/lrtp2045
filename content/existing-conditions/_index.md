---
title: Existing Conditions
date: 2018-09-18T13:50:42-06:00
draft: false
menu: main
weight: 20
bannerHeading: Existing Conditions
bannerText: >
  These data points build an understanding of the current context from which future plans can develop.
---

Using a base year of 2015, existing conditions data establish a baseline from
which planning visions can be grounded. This section includes demographic, land
use, environmental, and health data as well as information specific to each
transportation mode.
