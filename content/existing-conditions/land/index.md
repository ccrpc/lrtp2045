---
title: "Land Use"
date: 2018-09-18T13:50:42-06:00
draft: false
Weight: 20
bannerHeading: Land Use
bannerText: >
  Land use and planning principles are critical to understanding the impact of
  future transportation investments.
---

The distribution and concentration of people and land uses within the urban
area can increase or decrease transportation options for people and businesses
accessing goods, services, employment, education, open space, and other
resources. Effective coordination of land use development and transportation
planning can produce policies and strategies that preserve and enhance valued
natural and cultural resources and facilitate healthy, sustainable communities
and neighborhoods.

This section provides an overview of local land use information including
population density, land use distribution and acreage, and the locations of
archaeological and historical sites. Municipal and residential population density
are both documented in the following text to help illustrate how the
municipalities in the metropolitan planning area have developed since 1990.
Population density alone does not provide a complete picture of how different
municipalities grow and plan for future development. For a more in-depth picture
of local growth practices and policies, see planning documents specific to each
municipality.

## Population Density ##

### Municipal Population Density ###

The chart below shows municipal population density for the two cities and four
villages within the metropolitan planning area between 1990 and 2010 using land
area and population data from the U.S. Census Bureau. Overall, municipal
population density within the metropolitan planning area decreased by about 400
people per square mile between 1990 and 2010. The largest decreases were
observed in the Cities of Champaign and Urbana, with 1,289 and 1,109 fewer
people per square mile, respectively. A smaller decrease of 68 fewer people per
square mile was observed in the Village of Tolono. Population density increased
in the Village of Bondville and the Village of Savoy. Due to municipal
annexations for the purposes of planning and future development, even
municipalities with intentions of efficient, compact development could have
large undeveloped acreages within their corporate limits that make municipal
population density appear incongruous with current development practices or
future development goals.

<rpc-chart
  url="landuse-density-population.csv"
  type="line"
  switch="true"
  y-label="Municipal Density (Persons per Square Mile)"
  legend-position="top"
  legend="true"
  tooltip-intersect="true"
  source=" US Census Bureau, 1990-2010 Decennial Census, Table P1 Summary File 1 and Gazetteer Files"
  source-url="https://factfinder.census.gov/bkmk/table/1.0/en/DEC/10_SF1/P1/1600000US1707211|1600000US1712385|1600000US1746136|1600000US1767860|1600000US1775614|1600000US1777005"
  chart-title="Municipal Population Density, 1990-2010">
</rpc-chart>

### Residential Population Density ###
The residential population densities shown in the following chart were
calculated by dividing the ACS municipal population estimates by the total
residential land area. Total residential land area included tax assessor parcels
associated with single-family, multi-family, and mobile home land uses.
Residential density from 2007 to 2017 increased the most significantly in Savoy.
Champaign’s residential population density slightly increased from 2007 to 2012
and remained constant after 2012. Urbana and Bondville both experienced an
increase from 2007 to 2012 followed by decreases in residential population
density from 2012 to 2017. No data is available for Tolono or Mahomet from 2007
or 2012.

<rpc-chart
  url="landuse-density-residential.csv"
  type="bar"
  switch="true"
  y-label="Persons per Square Mile of Residential Land"
  legend-position="top"
  legend="true"
  tooltip-intersect="true"
  source=" Champaign County Tax Assessor, 2007-2017 Parcel-level Data; U.S. Census Bureau, 2008-2012 and 2013-2017 American Community Survey 5-Year Estimates, Table B01001; and Champaign County Regional Planning Commission, State of the County - 2010 Edition"
  chart-title="Residential Population Density, 2007-2017">
</rpc-chart>

The charts below show changes in the proportion of parcels associated with the
three residential categories in Champaign, Urbana, and Savoy from 2007 to 2017.
Single-family parcels account for the vest majority of residential parcels in
all three municipalities compared with multi-family parcels or mobile home
parcels. The lowest proportion of single-family parcels is still an overall
majority at 72 percent in Urbana in 2017. This time period demonstrates a modest
but consistent increase in the proportion of multi-family parcels in all three
municipalities. Parcels designated for mobile homes increased slightly in Urbana
between 2007 and 2017 but did not change significantly in Champaign or Savoy.

<rpc-chart
  url="landuse-residential2.csv"
  type="bar"
  switch="false"
  stacked="true"
  y-label="Percent"
  legend-position="top"
  legend="true"
  tooltip-intersect="true"
  source=" Champaign County, Tax Assessor, 2007-2017 Parcel-level Data"
  description="Note: Residential land area did not change significantly in Bondville from 2007 to 2017. Tolono and Mahomet did not have sufficient data to calculate land use change between 2007 and 2017.""
  chart-title="Residential Land Area by Category, 2007 and 2017">
</rpc-chart>

<rpc-chart
  url="landuse-residential-change2.csv"
  type="bar"
  switch="false"
  stacked="true"
  y-label="Percent"
  legend-position="top"
  legend="true"
  tooltip-intersect="true"
  source=" Champaign County, Tax Assessor, 2007-2017 Parcel-level Data"
  description="Note: Residential land area did not change significantly in Bondville from 2007 to 2017. Tolono and Mahomet did not have sufficient data to calculate land use change between 2007 and 2017.""
  chart-title="Residential Land Area, Proportional Change by Category, 2007-2017">
</rpc-chart>

## Land Use Distribution ##

The proportion of different land uses in the metropolitan planning area hasn't
changed significantly since 2010 according to data from the Champaign County Tax
Assessor. Land area designated for residential uses has increased approximately
two percent while land area designated for agriculture and other uses has
remained relatively stable.

<iframe src="https://maps.ccrpc.org/lrtp2045-parcel-land-use/"
  width="100%" height="600" allowfullscreen="true">
  Map displaying current land-use in the metropolitan planning area by
  parcel-level tax data.
</iframe>

<rpc-table
  url="landuse-area-byclass.csv"
  text-alignment="1,r"
  table-title="Land Use in the MPA, 2017"
  switch="false"
  source=" Champaign County, Tax Assessor, 2017 Parcel-level Data">
</rpc-table>

## Archaeological and Historical Sites ##

Archaeological and historical resources in the metropolitan planning area
include buildings, sites, districts, structures, and objects. The map below
displays locations and types of archaeological and historical resources, the
majority of which are found within or near the University of Illinois campus,
downtown Champaign, and downtown Urbana. Any new or proposed transportation
development needs to take measures to avoid adverse impacts such as damage,
destruction, or removal of these features. Furthermore, transportation-related
development should seek to preserve the larger setting of these structures when
the setting is a significant element of the property.

<iframe src="https://maps.ccrpc.org/lrtp2045-ecr-landuse-archaeological/"
  width="100%" height="600" allowfullscreen="true">
  Map displaying historic sites, buildings, and other archaeological places of interest within the metropolitan planning area.
</iframe>
