---
title: "Demographics"
date: 2018-09-18T13:50:42-06:00
draft: false
weight: 10
bannerHeading: Demographics
bannerText: >
  The Champaign-Urbana Urbanized Area is a diverse region spanning 47 square
  miles and containing a population of more than 148,000 people in east-central
  Illinois.
---

The Champaign-Urbana Urbanized Area spans approximately 47 square miles and
contains a population of more than 148,000 people in east-central Illinois. The
region is located 135 miles south of Chicago, Illinois, 120 miles west of
Indianapolis, Indiana, and 180 miles northeast of Saint Louis, Missouri. Five
municipalities are partially or wholly within the Champaign-Urbana Urbanized
Area, including: City of Champaign, City of Urbana, Village of Savoy, Village of
Tolono, and Village of Bondville. In contrast, the Metropolitan Planning Area
(MPA) is the 25-year planning area for the long range transportation plan. The
MPA spans 179 square miles and includes the Champaign-Urbana Urbanized Area, as
well as some additional area surrounding the urbanized area including the
Village of Mahomet. The function of the MPA boundary is to consider areas that
could be developed and included in the urbanized area over the next 25 years.

General characteristics of each municipality within the MPA are provided in the
table below. From the 2013 to 2017 American Community Survey (ACS) five-year
estimates, Savoy and Mahomet experienced the greatest population growth with
increases of 14.7 and 9.3 percent, respectively. Bondville and Tolono
populations decreased during the same timeframe. Total land area generally
remained the same or increased modestly for municipalities within the MPA. The
greatest change in land area occurred in Mahomet at an almost six percent
increase.

<rpc-table text-alignment="1,r"
url="basicdemographics2013-2017.csv"
table-title="Population and Land Area of MPA Municipalities, 2017"
switch="true"
source=" U.S. Census Bureau, 2006-2010 and 2013-2017 American Community Survey 5-Year Estimates, Table B01003, Accessed 13 February 2019. U.S. Census Bureau, 2014 and 2018 U.S. Gazetteer Files, Illinois Places, Accessed 22 February 2019"
source-url="https://factfinder.census.gov/bkmk/table/1.0/en/ACS/15_5YR/B01003/1600000US1707211|1600000US1712385|1600000US1746136|1600000US1767860|1600000US1775614|1600000US1777005">
</rpc-table>

<rpc-chart
url="chart-basicdemoinfo.csv"
type="bar"
colors="blue,red"
y-label="Percent Change"
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2006-2010 and 2013-2017 American Community Survey 5-Year Estimates, Table B01003, Accessed 13 February 2019. U.S. Census Bureau, 2014 and 2018 U.S. Gazetteer Files, Illinois Places, Accessed 22 February 2019"
source-url="https://www.census.gov/geo/maps-data/data/gazetteer2010.html"
chart-title="Percent Change in Population and Land Area, 2013-2017"></rpc-chart>

## Age and Gender ##

To identify how this region is unique, it can be useful to use ACS data to
compare local data with national data. The most significant difference between
local and national age distribution is the larger share of adults between the
ages of 18 and 24 in Champaign and Urbana compared with the U.S. overall due to
the University of Illinois. Another significant difference is the proportion of
adults 65 years and over living in Savoy compared with the US and other local
municipalities, due to residential and medical facilities geared toward older
adults located in Savoy.

<rpc-chart
url="acs17-age.csv"
type="bar"
stacked="true"
y-label="Percent of Population"
y-min="0"
y-max="100"
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2013-2017 American Community Survey 5-Year Estimates, Table B01001"
source-url="https://factfinder.census.gov/faces/tableservices/jsf/pages/productview.xhtml?pid=ACS_17_5YR_B01001&prodType=table"
chart-title="Percent Age Distribution in the MPA, 2017"></rpc-chart>


## People with Disabilities ##

Within the MPA, people with disabilities range from 7 to 16 percent of the each
municipality’s total population. Bondville’s percentage of people with
disabilities is the highest of all the MPA municipalities and higher than the US
overall. Savoy has the largest percentage of adults over the age of 65 with a
disability at over 40 percent of the population, followed by Tolono and Mahomet.

<rpc-chart
url="acs-disability2013-2017.csv"
type="bar"
stacked="true"
columns="1,2,3,4"
y-label="Percentage of Population"
legend-position="top"
legend="true"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2013-2017 American Community Survey 5-Year Estimates, Table B18101"
source-url="https://factfinder.census.gov/faces/tableservices/jsf/pages/productview.xhtml?pid=ACS_17_5YR_B18101&prodType=table"
chart-title="Percentage of the Population with a Disability, 2017"><rpc-dataset
type="line"
label="National Percentage of People with Disabilities"
data="12.6,12.6,12.6,12.6,12.6,12.6"
fill="false"
order="7"
border-color="gray"
point-radius="0"></rpc-dataset></rpc-chart>

## Race and Ethnicity ##

The racial and ethnic makeup of the MPA population is more diverse than the
nation overall. The percentage of minority racial groups in Champaign, Urbana,
and Savoy tends to be about 25 percent higher than those in Tolono, Bondville,
and Mahomet. The Hispanic or Latino population is more uniformly distributed
within each of the MPA municipalities, with a range of 3.7 percent of the
population in Tolono to 6.4 percent in Champaign. Since 2013, the percentage of
minority populations increased in all MPA municipalities with the exception of
Mahomet.

<rpc-chart
url="acs-raceethnicity2017.csv"
type="bar"
switch="false"
colors="blue,orange,red,green,indigo,lime,#c4c4c4"
rows="1,3,4,5,6,7,8"
stacked="true"
y-label="Percentage of Population"
y-min="0"
y-max="100"
legend-position="top"
legend="true"
tooltip-intersect="true"
grid-lines="true"
source=" U.S. Census Bureau, 2013-2017 American Community Survey 5-Year Estimates, Table B02001"
source-url="https://factfinder.census.gov/faces/tableservices/jsf/pages/productview.xhtml?pid=ACS_17_5YR_B02001&prodType=table"
chart-title="Race in MPA Municipalities, 2017"></rpc-chart>

<rpc-chart
url="acs-raceethnicity2017.csv"
type="bar"
colors="#c4c4c4,#24ABE2"
stacked="false"
grid-lines="false"
xangle="-0"
rows="16,17,18,19,20,21,22"
columns="1,2,3"
y-label="Percentage of Population"
legend-position="top"
legend="true"
tooltip-intersect="true"
grid-lines="false"
source=" U.S. Census Bureau, 2006-2010 and 2013-2017 American Community Survey 5-Year Estimates, Table B02001"
source-url="https://factfinder.census.gov/faces/tableservices/jsf/pages/productview.xhtml?pid=ACS_17_5YR_B02001&prodType=table"
chart-title="Race and Ethnicity in MPA Municipalities, 2013-2017">
<rpc-dataset
type="line"
fill="false"
border-color="#c4c4c4"
border-width="1"
point-radius="0"
order="3"
label="National Percentage of Minority Groups in 2013"
data="26,26,26,26,26,26">
</rpc-dataset><rpc-dataset
type="line"
fill="false"
border-color="#24ABE2"
border-width="1"
point-radius="0"
order="4"
label="National Percentage of Minority Groups in 2017"
data="27,27,27,27,27,27">
</rpc-dataset></rpc-chart>


## Income and Poverty ##

The population living in poverty has generally increased within the MPA
municipalities between the 2013 and 2017 according to the ACS. Urbana and
Champaign have the highest percentages of people living in poverty, exceeding
the national average by about 10 and 17 percent, respectively. Overall, Urbana,
Bondville, and Mahomet experienced a decrease in poverty rates, while Champaign
and Tolono sustained about the same poverty rates.

The high proportion of individuals living in poverty in Champaign, Urbana, and
Savoy can at least partially be attributed to the university student population
that largely subsides on student loans and other outside funding sources while
studying at the University of Illinois. In the Fall of 2017, 47,826
undergraduate, professional, and graduate students were enrolled at the
Urbana-Champaign campus.

<rpc-chart
url="acs-poverty2013-2017.csv"
type="bar"
stacked="false"
columns="1,2,3"
colors="#c4c4c4, #24ABE2"
legend-position="top"
legend="true"
y-label="Percentage of Population"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2010-2015 and 2013-2017 American Community Survey 5-Year Estimates, Table C17001"
source-url="https://factfinder.census.gov/faces/tableservices/jsf/pages/productview.xhtml?pid=ACS_17_5YR_B17001&prodType=table"
chart-title="Percentage of Population Living below the Poverty Level, 2013-2017">
<rpc-dataset
type="line"
fill="false"
border-color="#c4c4c4"
border-width="1"
point-radius="0"
order="3"
label="National Poverty Rate in 2013"
data="15.4,15.4,15.4,15.4,15.4,15.4">
</rpc-dataset><rpc-dataset
type="line"
fill="false"
border-color="#24ABE2"
border-width="1"
point-radius="0"
order="4"
label="National Poverty Rate in 2017"
data="14.6,14.6,14.6,14.6,14.6,14.6">
</rpc-dataset></rpc-chart>

The income to poverty ratio determines the extent to which a household’s income
is above or below the poverty threshold. An income to poverty ratio of less than
one corresponds with a household income below the poverty threshold, and an
income to poverty ratio of less than 0.50 represents a household income at less
than 50 percent of the poverty threshold. According to 2017 ACS data, about 32
percent of Urbana residents and over a quarter of Champaign residents subsisted
on income below half of the poverty threshold.

<rpc-chart
url="acs-poverty2013-2017.csv"
type="bar"
switch="false"
colors="red,#c77f87,gray,#c4c4c4,#DDDDDD"
columns="1,4,5,6,7,8"
grid-lines="false"
stacked="true"
legend-position="top"
legend="true"
y-label="Percentage of Population"
y-min="0"
y-max="100"
tooltip-intersect="true"
source=" U.S. Census Bureau, 2013-2017 American Community Survey 5-Year Estimates, Table C17002"
source-url="https://factfinder.census.gov/faces/tableservices/jsf/pages/productview.xhtml?pid=ACS_17_5YR_C17002&prodType=table"
chart-title="Income to Poverty Ratio in the MPA, 2017">
<rpc-dataset
type="line"
label="National Rate of Poverty"
data="14.6,14.6,14.6,14.6,14.6,14.6"
fill="false"
order="7"
border-color="#470b11"
point-radius="0"></rpc-dataset>
</rpc-chart>

Household access to motor vehicles is one dataset that can help illustrate the
relationship between income and mobility. See [Automobile
Ownership](https://ccrpc.gitlab.io/lrtp2045/existing-conditions/transportation/#automobile-ownership)
for more information.

## Population Distribution and Density ##

Changes in the urbanized area boundary occur every ten years in conjunction with
the decennial census. Since the last LRTP update, there have been no updates to
the urbanized area boundary. The urbanized area grew from 123,938 people in 2000
to 145,361 people in 2010. The urbanized area also grew by more than eight
square miles from 2000 to 2010, with a continued trend of nearly a three-percent
shift from rural to urban land designation per decade in Champaign County from
1980 to 2010.

<rpc-table url="urban-rural-ratio1980-2010.csv"
table-title="Urban-Rural Ratio (1980-2010)"
text-alignment="1,r"
switch="false"
source=" U.S. Census Bureau, 1980-2010 Decennial Census, Summary File 1 Table P2"></rpc-table>

## Employment ##

Champaign County is a regional employment center because of the presence of
education, health care, and manufacturing employers in the area. According to
the Illinois Department of Employment Security (IDES), the unemployment rate
decreased from an average of 9 to 5 percent during the period between 2013 and
2017. The Census Bureau’s Quarterly Workforce Indicators identified the
following top five employments sectors for Champaign County in 2018:

1.	Educational Services, 18,803 workers
2.	Health Care and Social Assistance,12,850 workers
3.	Accommodation and Food Services, 9,350 workers
4.	Retail Trade, 8,467 workers
5.	Manufacturing, 6,627 workers

The following tables lists Champaign County’s top employers and the change in
their numbers of employees since 2013. The University of Illinois experienced
the biggest growth of 3,114 employees from 2013 to 2018 followed by Carle and
FedEx. Kraft Foods, Inc., Parkland College, OSF Heart of Mary Medical Center,
and Urbana School District #116 reduced the number of people they employ during
that same time period.

<rpc-table
url="edc-topemployers.csv"
text-alignment="1,r"
table-title="Champaign County Top Employers (2018)"
switch="false"
source-url="http://www.champaigncountyedc.org/area-facts/directories-reports"
source=" Champaign County Economic Development Corporation, 2013 and 2018 Top Employers - Champaign County, Illinois, Accessed 20 February 2019">
</rpc-table>
