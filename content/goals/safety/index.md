---
title: "Safety"
date: 2018-09-18T13:50:42-06:00
draft: false
Weight: 20
bannerHeading: Safety
bannerText: >
  The metropolitan planning area will maintain, preserve, and operate its existing transportation system to provide safe, efficient, and reliable movement of people, goods, and services in the short term, and in the long term, achieve the state’s goal of zero deaths and disabling injuries.
---

## Goal

The metropolitan planning area will maintain, preserve, and operate its
existing transportation system to provide safe, efficient, and reliable movement
of people, goods, and services in the short term, and in the long term, achieve
the state’s goal of zero deaths and disabling injuries.

## Objectives and Performance Measures

<rpc-table url="safety_table.csv"
  text-alignment="c,c"></rpc-table>

***Visit the [Annual LRTP 2045 Report
Card](https://data.ccrpc.org/pages/lrtp-2045-report-card) to check the status of
the performance measures for the active years of the plan, 2020 to 2024, with
2020 as the base year.***

## Strategies

1.	Implement the strategies approved in the Champaign-Urbana Urban Area Safety Plan
for safety emphasis areas, including intersections, pedestrians, bicyclists, and
impaired driving.
**Responsible Parties:** CUUATS staff, IDOT, Champaign County, local cities, villages and townships (Public Works), MTD, CU SRTS Project, University of Illinois, local and state law enforcement agencies, local EMS and hospitals, judiciary system

2.	Continue to facilitate regional Safety Committee to support collaboration between planners, engineers, law enforcement, and other community partners.
**Responsible Parties:** CUUATS staff, IDOT, Champaign County, local cities, villages and townships (Public Works), MTD, CU SRTS Project, University of Illinois, local and state law enforcement agencies, local EMS and hospitals, judiciary system

3.	Promote safety in the planning, design, construction, and maintenance of all modes in transportation projects and programs (e.g., designing for the incorporation of emerging safety-related technologies).
**Responsible Parties:** CUUATS, IDOT, Cities, Villages, MTD, University of Illinois

4.	Prepare applications and provide input to local agencies regarding Highway Safety Improvement Program (HSIP) funds.
**Responsible Parties:** CUUATS staff

5.	Complete applications for available Federal safety funding.
**Responsible Parties:** CUUATS staff

6.	Evaluate HSIP projects by completing before and after studies.
**Responsible Parties:** IDOT

7.	Continue to enforce codes requiring new development to provide sidewalks along roadway frontages and safe crossings at intersections.
**Responsible Parties:** CUUATS Staff, Cities and Villages, Developers, University of Illinois

9.	Continue to provide and upgrade sidewalk infrastructure according to ADA standards.
**Responsible Parties:** CUUATS Staff, Cities and Villages, University of Illinois

10.	Revise, complete and distribute Safe Walking Route Maps for public elementary and middle schools in Champaign-Urbana every two years and continue the Safe Routes to School program.
**Responsible Parties:** CUUATS staff, C-U SRTS Project

11.	Work with municipalities and transportation study groups to evaluate existing speed limits on the local roadway network.
**Responsible Parties:** CUUATS staff

12.	Perform Road Safety Audit (RSA) at the request of local agencies, maintain list of trained volunteers to help conduct RSAs.
**Responsible Parties:** CUUATS staff

13.	Assess the regional transportation network for safe routing of hazardous materials and designate the most appropriate routes for hazmat transportation.
**Responsible Parties:** CUUATS staff, Cities and Villages, Champaign County Emergency Management Agency (EMA), Developers, Champaign County LEPC, law enforcement, CUMTD, University of Illinois

14.	Finish updating the regional Intelligent Transportation System (ITS) architecture and install Vehicle Management Systems (VMS) at major roadways and intersections when appropriate.
**Responsible Parties:** CUUATS staff, Cities and Villages, Champaign County Emergency Management Agency (EMA), Developers, Champaign County LEPC, law enforcement, CUMTD, University of Illinois

15.	Perform periodic emergency evacuation drills at different agencies including local school districts.
**Responsible Parties:** CUUATS staff, Cities and Villages, Champaign County Emergency Management Agency (EMA), Developers, Champaign County LEPC, law enforcement, CUMTD, University of Illinois

16.	Create an evacuation plan for the region that would set the regional transportation system to be ready for efficiently performing evacuation in case of a natural or man-made disaster.
**Responsible Parties:** CUUATS staff, Cities and Villages, Champaign County EMA, LEPC, school districts, law enforcement, MTD

17.	Coordinate with IDOT, Department of Homeland Security (DHS), and local agencies to ensure that up to date security features are installed at relevant regional transportation infrastructure.
**Responsible Parties:** CUUATS staff, DHS, IDOT, law enforcement, Cities and Villages, CUMTD, University of Illinois

18.	Ensure robust and meaningful community engagement for communities to identify and develop solutions to the transition to a future with automated vehicles (AVs). This includes community engagement in developing AV regulations.
**Responsible Parties:** IDOT, CUUATS, Cities, Villages, University of Illinois, MTD
