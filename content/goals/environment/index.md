---
title: "Environment"
date: 2018-09-18T13:50:42-06:00
draft: false
Weight: 60
bannerHeading: Environment
bannerText: >
  The metropolitan planning area will reduce the transportation system’s significant contribution to greenhouse gas emissions and environmental degradation to maintain a high quality of human and environmental health in the region.

---

## Goal
The metropolitan planning area will reduce the transportation system’s
significant contribution to greenhouse gas emissions and environmental
degradation to maintain a high quality of human and environmental health in the
region.

## Objectives and Performance Measures
<rpc-table url="environment_table.csv"
  text-alignment="c,c"></rpc-table>

***Visit the [Annual LRTP 2045 Report
Card](https://data.ccrpc.org/pages/lrtp-2045-report-card) to check the status of
the performance measures for the active years of the plan, 2020 to 2024, with
2020 as the base year.***

## Strategies

1.	Finish developing the Champaign County Ecological Framework and maintain the regional ecological database to aid in identifying and mitigating negative environmental impacts of transportation projects.
    **Responsible Parties:** CUUATS staff

2.	Develop a regional land use map and identify approaches to maintaining and updating the map.
    **Responsible Parties:** CCRPC/CUUATS staff

3.	Quantify the agricultural system’s contribution to the regional and local economies to better inform local economic development strategies, land use planning, and transportation investments.
    **Responsible Parties:** CUUATS, Champaign County Farm Bureau, Champaign County EDC, Champaign County Chamber of Commerce

4.	Encourage compact development practices.
    **Responsible Parties:** Cities, Villages

5.	Install alternative fueling and charging stations in areas of the community that don't already have them.
    **Responsible Parties:** Local municipalities and Public Works Departments

6.	Plan for integration of solar and charging stations into new roadway and development projects.
    **Responsible Parties:** Cities, Villages, University of Illinois, CUUATS staff

8.	Continue to implement recommendations listed in the Transit Facility Guidelines for the Champaign-Urbana Urbanized Area.
    **Responsible Parties:** MTD

9.	Continue to implement proposed improvements to bicycle infrastructure approved in regional and local bicycle, and greenways and trails plans.
    **Responsible Parties:** CUUATS Staff, Cities and Villages, University of Illinois

10.	Increase accessibility to transit services by providing missing sidewalk connections.
    **Responsible Parties:** Cities and Villages Public Works Departments, University of Illinois, IDOT

11.	Continue to provide and upgrade sidewalk infrastructure according to ADA standards.
    **Responsible Parties:** CUUATS Staff, Cities and Villages, University of Illinois

12.	Continue annual sidewalk inventory and assessment.
    **Responsible Parties:** CUUATS Staff, Cities, Villages, University of Illinois

13.	Promote active modes of transportation through various forms of encouragement (online materials, educational events, signage, etc.)
    **Responsible Parties:** Cities, Villages, University of Illinois, MTD, CUUATS

14.	Create and publicize community-wide calendar of events including Bike To Work Day, Walk n’ Roll to School Day, the Christie Clinic Illinois Marathon, and more.
    **Responsible Parties:** CUUATS, C-U SRTS Project, local bike groups, all local municipalities, local Park Districts, Champaign County Forest Preserve District

15.	Create new bike encouragement events like, Bike-N-Dine, Polar Bear Bike Ride, Smart Trip Planning, or Spring Into Action Walk About.
    **Responsible Parties:** CUUATS, C-U SRTS Project, local bike groups, all local municipalities, local Park Districts

16.	Produce and distribute a regularly updated map including bike, trail, and park facilities.
    **Responsible Parties:** CUUATS, C-U SRTS Project, local bike groups, all local municipalities, local Park Districts

17.	Utilize data obtained from Access Score to inform future development locations and recommendations.
    **Responsible Parties:** Cities, Villages

18.	Provide Complete Street connections to and between downtowns.
    **Responsible Parties:** Cities of Champaign and Urbana, CUUATS Staff

19.	Consider and avoid negative impacts of new and existing transportation projects on historically significant buildings, landmarks, districts, and/or structures.
    **Responsible Parties:** Cities, Villages, IDOT, CCRPC Staff

21.	Adopt conservation-oriented development standards that avoid development of key natural areas and ensure long-term stewardship of natural areas and open space.
    **Responsible Parties:** Cities and Villages

22.	Consider existing road, water, and wastewater infrastructure capacity in decisions about the intensity and extent of new development.
    **Responsible Parties:** Cities and Villages

23.	Reduce or eliminate minimum parking requirements, or set maximum parking limitations in some locations, such as near transit.
    **Responsible Parties:** Cities and Villages

24.	Incorporate green infrastructure and other green strategies into neighborhood parks, school yards and properties, corporate and office campuses, and other open lands.
    **Responsible Parties:** Cities, Villages,  Park Districts, School Districts, private sector, developers

25.	Identify an approach to incorporate climate resilience and adaptation measures into transportation projects and planning.
    **Responsible Parties:** Cities, Villages, University of Illinois, Champaign County Planning and Zoning, CCRPC

26.	Determine the vulnerability of transportation infrastructure to climate change impacts and design transportation infrastructure to withstand and adapt to the climate of its intended lifespan.
    **Responsible Parties:** IDOT, CUUATS Staff, Cities, Villages, University of Illinois, MTD

27.	Conduct an analysis of road performance under severe weather conditions to develop planned responses.
    **Responsible Parties:** IDOT, CUUATS Staff, Cities, Villages, University of Illinois, MTD

28.	Develop a regional pavement flooding reporting system to help plan for flood events.
    **Responsible Parties:** IDOT, CUUATS Staff, Cities, Villages, University of Illinois, MTD

29.	Encourage zero emission automated vehicles (AVs) with clean energy sources, to reduce air quality impacts.
    **Responsible Parties:** IDOT, CUUATS, Cities, Villages, University of Illinois, MTD

30.	Minimize potential sprawl induced by autonomous vehicles (AVs).
    **Responsible Parties:** IDOT, CUUATS, Cities, Villages, University of Illinois, MTD

31. conduct a study to identify and implement best practices to avoid the impacts of road salt as non-point source pollution.
    **Responsible Parties:** CUUATS, Cities, Villages
