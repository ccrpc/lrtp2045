---
title: "Multimodal Connectivity"
date: 2018-09-18T13:50:42-06:00
draft: false
Weight: 30
bannerHeading: Multimodal Connectivity
bannerText: >
  The Champaign-Urbana area will increase accessibility, connectivity, and mobility of people and freight to all areas of the region by maintaining a multimodal system of transportation that is cost-effective for people, businesses, and institutions and will allow freedom of choice in all modes of transportation including active modes whenever possible.
---

## Goal
The Champaign-Urbana area will increase accessibility, connectivity, and
mobility of people and freight to all areas of the region by maintaining a
multimodal system of transportation that is cost-effective for people,
businesses, and institutions and will allow freedom of choice in all modes of
transportation including active modes whenever possible.

## Objectives and Performance Measures
<rpc-table url="mutlimodal_table.csv"
  text-alignment="c,c"></rpc-table>

***Visit the [Annual LRTP 2045 Report
Card](https://data.ccrpc.org/pages/lrtp-2045-report-card) to check the status of
the performance measures for the active years of the plan, 2020 to 2024, with
2020 as the base year.***

## Strategies

1.	Continue to implement proposed improvements to bicycle infrastructure approved in regional and local bicycle, and greenways and trails plans.
**Responsible Parties:** CUUATS Staff, Cities and Villages, Developers, University of Illinois

2.	Continue to enforce codes requiring new development to provide sidewalks along roadway frontages and safe crossings at intersections.
**Responsible Parties:** CUUATS Staff, Cities and Villages, Developers, University of Illinois

3.	Continue to provide and upgrade sidewalk infrastructure according to ADA standards.
**Responsible Parties:** CUUATS Staff, Cities and Villages, University of Illinois

4.	Implement strategies proposed in the Sidewalk Network Inventory and Assessment for the Champaign-Urbana Urbanized Area, including recommendations under the categories of Compliance, Condition, Connectivity, Priority Areas and Funding.
**Responsible Parties:** Cities, Villages, and University of Illinois

5.	Revise, complete and distribute Safe Walking Route Maps for public elementary and middle schools in Champaign-Urbana every two years and continue the Safe Routes to School program.
**Responsible Parties:** CUUATS staff, C-U SRTS Project

6.	To maximize pedestrian, bicycle, and transit improvement implementation efforts, identify ways of including bicycle, pedestrian, and transit accommodations into all new roadway projects that don't already consider other modes.
**Responsible Parties:** IDOT, Cities, Villages, University of Illinois, Developers, Townships

7.	Consult with existing bicycle, pedestrian, and greenways and trails plans and local agencies to coordinate infrastructure priorities.
**Responsible Parties:** IDOT, Cities, Villages, and University of Illinois

8.	Take advantage of opportunities to develop off-street shared-use paths, using methods including but not limited to: working with railroads to develop bicycle facilities on or along rights-of-way, and acquiring property that provides off-street connections between bicycle facilities.
**Responsible Parties:** Cities, Villages, University of Illinois, Park Districts of Champaign and Urbana, Champaign County Forest Preserve District, Developers

9.	Explore private/ public partnerships to implement multimodal infrastructure improvements in specific locations that benefit surrounding businesses.
**Responsible Parties:** Cities, Villages, University of Illinois, IDOT

10.	Coordinate with local law enforcement regarding new pedestrian and bicycle plans and associated education and enforcement activities, especially targeting motorists.
**Responsible Parties:** Police, Municipalities, University of Illinois, IDOT

11.	Develop stronger active transportation connections between downtown Champaign and downtown Urbana as recommended in IDOT’s 2015 University Avenue Road Safety Assessment and the 2010 University Avenue Corridor Study.
**Responsible Parties:** IDOT, Cities and University of Illinois

12.	Create active transportation connections between Savoy, the University Research Park, and the University of Illinois campus.
**Responsible Parties:** Village of Savoy, University of Illinois

13.	Connect underserved rural transit areas by linking rural transit services to urban transit service routes at connecting points.
**Responsible Parties:** MTD, CCARTS

14.	Annex additional urbanized area land into the MTD service area.
**Responsible Parties:** MTD

15.	Provide transit service to areas of new residential, commercial and/or industrial development.
**Responsible Parties:** MTD

16.	Evaluate existing routes and service times to determine if transit service is meeting resident and worker needs.
**Responsible Parties:** MTD

17.	Coordinate with Amtrak to provide more consistent fare pricing.
**Responsible Parties:** Amtrak

18.	Create at least 2 new regional or national flight connections that match nearby airport destinations or are unique destinations to the region to increase the appeal of Willard Airport to travelers.
**Responsible Parties:** Willard Airport, University of Illinois, MTD

19.	Identify cost effective ways to provide transit service to Willard Airport.
**Responsible Parties:** Willard Airport, University of Illinois, MTD

20.	Identify opportunities to install wayfinding signage for all modes of transportation.
**Responsible Parties:** CUUATS member agencies, Visit Champaign County

21.	Support efforts by IDOT, the Midwest High Speed Rail Association, and other related entities to advance the case for a Chicago-Champaign-St. Louis high speed rail corridor.
**Responsible Parties:** High speed rail consortium or IDOT

22.	Use community-wide calendars to promote multimodal transportation to existing events.
**Responsible Parties:** CUUATS Staff

23.	Continue to market the benefits of lifestyles free of car ownership to existing and future students and residents.
**Responsible Parties:** CUMTD, Cities, Villages, and University of Illinois

24.	Set up information tables for transportation-related public outreach at popular events listed on municipal calendars of public events (i.e. Neighborhood Nights, Sounds at Sunset, Orchard Days, RC Fest, sporting events, etc.).
**Responsible Parties:** CUUATS member agencies and all local municipalities

25.	Distribute at least 1 type of educational and/or encouragement material related to transportation modes, facilities, and benefits to K-12 schools.
**Responsible Parties:** CUUATS member agencies and all local municipalities

26.	Adopt CUUATS Access Management Guidelines into municipal codes or ordinances.
**Responsible Parties:** Cities, Villages, University of Illinois

27.	Adhere to the CUUATS Complete Streets Policy for all new and reconstruction transportation infrastructure projects
**Responsible Parties:** Cities, Villages, University of Illinois

28.	Apply for Illinois Transportation Enhancement Program (ITEP) grants when feasible to fund pedestrian and bicycle infrastructure projects.
**Responsible Parties:** Cities and Villages Public Works Departments

29.	Plan additional industrial subdivisions to efficiently connect with existing transportation infrastructure (e.g., sidewalks, bike paths, transit, roads, freight routes, etc.).
**Responsible Parties:** Cities, Villages, IDOT, CUUATS Staff

30.	Improve region-wide data on bicycle and pedestrian infrastructure usage and travel patterns.
**Responsible Parties:** CUUATS Staff, Cities, and Villages

31.	Acquire and analyze regional bike-share data.
**Responsible Parties:** CUUATS Staff, Cities, Villages, Private Sector

32.	Help local agencies and the public to identify the potential benefits and pitfalls of new mobility technologies, including autonomous and connected vehicles, with regard to safety, economic competitiveness, affordable mobility, accessibility, and environmental impacts.
**Responsible Parties:** IDOT, CUUATS Staff

33.	Convene and coordinate regional stakeholders to engage in national, state, and regional-level conversations about automated and connected vehicle policy and industry standards.
**Responsible Parties:** IDOT, CUUATS Staff, MTD

34.	Establish localized policies for automated vehicles (AVs), Transportation Network Companies (TNCs), and other emerging technologies that support multimodal connectivity goals.
**Responsible Parties:** CUUATS Staff, IDOT, Cities, Villages, University of Illinois, MTD

35.	Pilot new approaches for integrating automated vehicles (AVs) with strategies established to support and complement public transit and preserve safe, vibrant, equitable, accessible, and walkable communities. 	 
**Responsible Parties:** CUUATS Staff, IDOT, Cities, Villages, University of Illinois, MTD

36.	Coordinate with other public agencies and partners in academia to develop analytical tools and track the impact of emerging technologies. 	
**Responsible Parties:** CUUATS Staff, IDOT

38.	Encourage schools to work with municipal departments to implement engineering and enforcement recommendations proposed in SRTS plans.
**Responsible Parties:** Champaign School District, Urbana School District, Cities of Champaign and Urbana, C-U SRTS Project, CUUATS

39.	Work with local agencies to define encouragement and enforcement measures for snow removal.
**Responsible Parties:** Local municipalities and Public Works Departments

40.	Create new bike encouragement events like, Bike-N-Dine, Polar Bear Bike Ride, Smart Trip Planning, or Spring Into Action Walk About.
    **Responsible Parties:** CUUATS, C-U SRTS Project, local bike groups, all local municipalities, local Park Districts

41.	Produce and distribute a regularly updated map including bike, trail, and park facilities.
    **Responsible Parties:** CUUATS, C-U SRTS Project, local bike groups, all local municipalities, local Park Districts
