---
title: "Measuring Progress"
date: 2018-09-18T13:50:42-06:00
draft: false
Weight: 10
bannerHeading: Measuring Progress
bannerText: >
  The formulation of goals and objectives determines what direction planning efforts should take.
---

The [Champaign Urbana Urbanized Area Transportation Study
(CUUATS)](https://ccrpc.org/programs/transportation/) staff, in conjunction with
the Long Range Transportation Plan (LRTP) Steering Committee, developed goals
with related objectives and performance measures to reflect the long range
transportation vision for 2045 and provide direction for the plan’s
implementation. The five overarching long-term goals are **safety, multimodal
connectivity, equity, economy, and environment**. These goals and their
objectives are based on a combination of the Federal transportation goals, State
of Illinois transportation goals, local knowledge, current local planning
efforts, and input received from the public. Additionally, short-term objectives
and performance measures translate the long range vision for 2045 into short
term action. [CUUATS](https://ccrpc.org/programs/transportation/) staff uses the
SMART (specific, measurable, agreed, realistic, and timebound) acronym to guide
the objective development process.

•	***Goals*** are defined as an end state that will be brought about by implementing
the plan.

•	***Objectives*** are sub-goals that help organize the plan implementation into
manageable parts.

•	***Performance Measures*** are metrics used to help agencies track the progress toward
each objective over time.

•	***Strategies*** are ideas or guiding principles to help agencies reach the stated goals and objectives.

## Annual LRTP Report Card

Each year [CUUATS](https://ccrpc.org/programs/transportation/) staff updates the
Annual LRTP Report Card to measure and document the progress made toward the
current LRTP’s short-term objectives using the performance measures. The Annual
LRTP Report Card helps provide transparency and continuity between five-year
LRTP updates by identifying opportunities and barriers to achieving LRTP goals
and objectives. Each objective’s performance measure (or measures) are assigned
a good, neutral, or negative rating depending on the circumstances and data
trend of each measure. The Report Card is reviewed by the
[CUUATS](https://ccrpc.org/programs/transportation/)
[Technical](https://ccrpc.org/committees/cuuats-technical/)
[Policy](https://ccrpc.org/committees/cuuats-policy/) Committees each year and
is available online for local agencies and members of the public.

The [Annual LRTP 2045 Report
Card](https://data.ccrpc.org/pages/lrtp-2045-report-card) tracks the
performance measures in the LRTP 2045 for the active years of the plan, 2020 to
2024, with 2020 as the base year. The [Annual LRTP 2040 Report
Card](https://reportcard.cuuats.org/) tracks the performance measures in the
LRTP 2040 for the years 2015 through 2019, with 2015 as the base year.

In addition to helping the metropolitan planning area refine its vision, the
LRTP 2045 has been updated in accordance with federal requirements. In May 2016,
the Federal Highway Administration (FHWA) and Federal Transit Administration
(FTA) jointly issued the final rule on Statewide and Nonmetropolitan
Transportation Planning and Metropolitan Transportation Planning, implementing
changes to the planning processes that had been established by the Moving Ahead
for Progress in the 21st Century Act (MAP21) and the Fixing America’s Surface
Transportation Act (FAST Act). These changes established 23 performance measures
intended to ensure effective investment of federal transportation funds. The
federal measures include five safety measures which are reflected in the LRTP
2045 [safety measures](https://ccrpc.gitlab.io/lrtp2045/goals/safety/), as well
as six infrastructure, three system performance, and nine transit asset
conditions measures. These additional measures are reflected in CUUATS
Annual Transportation Improvement Program (TIP) and are also
documented in this plan in the [System Performance
Report](https://ccrpc.gitlab.io/lrtp2045/data/tpm/). The [System Performance
Report](https://ccrpc.gitlab.io/lrtp2045/data/tpm/) includes an evaluation
of system performance with respect to each performance target.

{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title="National Transportation Goals"%}}
  **Safety:** To achieve a significant reduction in traffic fatalities and serious injuries on all public roads.    

  **Infrastructure Condition:** To maintain the highway infrastructure asset system in a state of good repair.

  **Congestion Reduction:** To achieve a significant reduction in congestion on the National Highway System.

  **System Reliability:** To improve the efficiency of the surface transportation system.

  **Freight Movement and Economic Vitality:** To improve the National Highway Freight Network, strengthen the ability of rural communities to access national and international trade markets, and support regional economic development.

  **Environmental Sustainability:** To enhance the performance of the transportation system while protecting and enhancing the natural environment.

  **Reduced Project Delivery Delays:** To reduce project costs, promote jobs and the economy, and expedite the movement of people and goods by accelerating project completion through eliminating delays in the project development and delivery process, including reducing regulatory burdens and improving agencies' work practices.

  *Source: United States Code, 23 U.S.C. 150(b)*
  {{%/accordion-content%}}
  {{</accordion>}}

  {{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title="State Transportation Goals"%}}
  **Economy:** Improve Illinois’ economy by providing transportation infrastructure that supports the efficient movement of people and goods.

  **Access:** Support all modes of transportation to improve accessibility and safety by improving connections between all modes of transportation.

  **Livability:** Enhance quality of life across the state by ensuring that transportation investments advance local goals, provide multimodal options and preserve the environment.

  **Stewardship:** Safeguard existing funding the increase revenues to support system maintenance, modernization and strategic growth of Illinois’ transportation system.

  **Resilience:** Proactively assess, plan, and invest in the state’s transportation system to ensure that our infrastructure is prepared to sustain and recover from extreme weather events and other disruptions.

  *Source: [Illinois Department of Transportation, Long Range Transportation Plan, 2019](http://idot.illinois.gov/transportation-system/transportation-management/planning/lrtp/index)*
  {{%/accordion-content%}}
{{</accordion>}}

{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title="LRTP 2045 Goals"%}}
  **Safety:** The metropolitan planning area will maintain, preserve, and operate its
  existing transportation system to provide safe, efficient, and reliable movement
  of people, goods, and services in the short term, and in the long term, achieve
  the state’s goal of zero deaths and disabling injuries.    

  **Multimodal Connectivity:** The Champaign-Urbana area will increase accessibility, connectivity, and
  mobility of people and freight to all areas of the region by maintaining a
  multimodal system of transportation that is cost-effective for people,
  businesses, and institutions and will allow freedom of choice in all modes of
  transportation including active modes whenever possible.

  **Equity:** The metropolitan planning area will aim to provide safe, affordable, and efficient transportation choices to all people to support a high quality of life in every part of the community.

  **Economy:**  The metropolitan planning area will maximize the efficiency of the local transportation network for area employers, employees, materials, products, and clients at the local, regional, national, and global levels and facilitate strong inter-regional collaborations to realize large-scale transportation improvements.

  **Environment:** The metropolitan planning area will reduce the transportation system’s significant contribution to greenhouse gas emissions and environmental degradation to maintain a high quality of human and environmental health in the region.

  {{%/accordion-content%}}
{{</accordion>}}
