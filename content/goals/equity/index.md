---
title: "Equity"
date: 2018-09-18T13:50:42-06:00
draft: false
Weight: 40
bannerHeading: Equity
bannerText: >
  The metropolitan planning area will aim to provide safe, affordable, and efficient transportation choices to all people to support a high quality of life in every part of the community.

---

## Goal
The metropolitan planning area will aim to provide safe, affordable, and
efficient transportation choices to all people to support a high quality of life
in every part of the community.

## Objectives and Performance Measures

<rpc-table url="equity_table.csv"
  text-alignment="c,c"></rpc-table>

***Visit the [Annual LRTP 2045 Report
Card](https://data.ccrpc.org/pages/lrtp-2045-report-card) to check the status of
the performance measures for the active years of the plan, 2020 to 2024, with
2020 as the base year.***

## Strategies

1.	Use Access Score to identify specific strategies to improve mobility and accessibility in new transportation planning and development efforts.
    **Responsible Parties:** CUUATS Staff, Cities and Villages, University of Illinois

2.	Implement strategies proposed in the Sidewalk Network Inventory and Assessment for the Champaign-Urbana Urbanized Area, including recommendations under the categories of Compliance, Condition, Connectivity, and Priority Areas and Funding.
    **Responsible Parties:** Cities, Villages, and University of Illinois

3.	Install new bike self-repair stations in areas that don't already have any.
    **Responsible Parties:** Cities, Villages, and University of Illinois, Park Districts, Champaign County Forest Preserve District

4.	Investigate what percentage of annual transit pass cost would be cost-effective to price an affordable, annual transit pass for high school-aged youth.
    **Responsible Parties:** MTD

5.	Investigate the feasibility of allowing middle and high-school aged students to use their school IDs to ride the bus for free at any time.
    **Responsible Parties:** MTD

6.	Evaluate existing routes and service times to determine if transit service is meeting resident and/or worker demands, particularly in low-income areas, and identify areas for expansion of service where needed.
    **Responsible Parties:** MTD

7.	Continue to implement recommendations listed in the Transit Facility Guidelines.
    **Responsible Parties:** MTD

8.	Encourage Amtrak to create lower and more consistent fares.
    **Responsible Parties:** Amtrak

9.	Increase Amtrak service frequencies.
    **Responsible Parties:** Amtrak

10.	Utilize data obtained from Access Score to inform future development locations and recommendations.
    **Responsible Parties:** CUUATS Staff, Cities and Villages

11.	Develop a regional transportation directory.
    **Responsible Parties:** CUUATS Staff, Cities and Villages

12.	Identify Economically Disconnected and Disinvested areas in the region while also considering transportation accessibility utilizing tools such as Access Score.
    **Responsible Parties:** CUUATS Staff

13.	Promote strategic investment in disinvested areas.
    **Responsible Parties:** Champaign County Chamber of Commerce, Champaign County EDC, Cities, and Villages, CCRPC

14.	Work with financial institutions to apply for low cost loans for infrastructure projects that qualify under the Community Reinvestment Act (CRA).
    **Responsible Parties:** Cities and Villages

15.	Support projects that improve commute options for low-income workers.
    **Responsible Parties:** CCRPC Staff, IDOT, Cities and Villages, University of Illinois, Champaign County EDC, Champaign County Chamber of Commerce

16.	Continue to pursue federal and state grant and other funding opportunities when and where appropriate.
    **Responsible Parties:** CUUATS Staff

17.	Develop guidance to ensure that any partnerships with private mobility services provide clear public benefits, are accessible to people with disabilities, and include protections for low income communities against sudden changes in the private market.
    **Responsible Parties:** CCRPC and MTD

18.	Hold project open houses when appropriate and make project materials available for the public.
    **Responsible Parties:** CUUATS member agencies

19.	Organize project advisory committees and invite all relevant stakeholders to sit at the table.
    **Responsible Parties:** CUUATS member agencies

20.	Use emerging technologies such as autonomous vehicles to build capacity for transit service and advance equity.
    **Responsible Parties:** CUUATS, MTD

21.	Ensure that emerging transportation technologies like autonomous vehicles are rolled out in a way that prevents displacement or disruption of active transportation.
    **Responsible Parties:** IDOT, CUUATS, Cities, Villages, University of Illinois, MTD

22.	Work to prevent any new autonomous vehicle infrastructure from segregating neighborhoods.
    **Responsible Parties:** IDOT, CUUATS, Cities, Villages, University of Illinois, MTD

23.	Ensure robust and meaningful community engagement for communities to identify and develop solutions to the transition to an AV future. This includes community engagement in developing AV regulations.
    **Responsible Parties:** IDOT, CUUATS, Cities, Villages, University of Illinois, MTD

24.	Update and maintain the Health Impact Assessment for the region with local health data to inform transportation investment priorities and support active transportation and physical activity in locations with high obesity and related disease rates.
    **Responsible Parties:** CUUATS, CUPHD, Healthcare providers
