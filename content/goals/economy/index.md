---
title: "Economy"
date: 2018-09-18T13:50:42-06:00
draft: false
Weight: 50
bannerHeading: Economy
bannerText: >
  The metropolitan planning area will maximize the efficiency of the local transportation network for area employers, employees, materials, products, and clients at the local, regional, national, and global levels and facilitate strong inter-regional collaborations to realize large-scale transportation improvements.

---

## Goal

The metropolitan planning area will maximize the efficiency of the local
transportation network for area employers, employees, materials, products, and
clients at the local, regional, national, and global levels and facilitate
strong inter-regional collaborations to realize large-scale transportation
improvements.

## Objectives and Performance Measures
<rpc-table url="economy_table.csv"
  text-alignment="c,c"></rpc-table>

***Visit the [Annual LRTP 2045 Report
Card](https://data.ccrpc.org/pages/lrtp-2045-report-card) to check the status of
the performance measures for the active years of the plan, 2020 to 2024, with
2020 as the base year.***

## Strategies

1.	Favor transportation policies and projects that result in greater job creation.
    **Responsible Parties:** CCRPC Staff, IDOT, Cities and Villages, University of Illinois, Champaign County EDC, Champaign County Chamber of Commerce

2.	Integrate transportation and land use planning to maximize the supply of development that can occur in accessible, multi-modal areas, in conjunction with pricing reforms that favor accessible locations.
    **Responsible Parties:** CCRPC Staff, Cities and Villages, Developers

4.	Plan additional industrial subdivisions to efficiently connect with existing transportation infrastructure (e.g., sidewalks, bike paths, transit, roads, freight routes, etc.).
    **Responsible Parties:** Cities, Villages, IDOT, CUUATS Staff

5.	Improve key transportation facilities and services connecting the region to national and international markets.
    **Responsible Parties:** CUUATS Staff, IDOT, Cities and Villages, University of Illinois, Champaign County EDC, Champaign County Chamber of Commerce

6.	Support projects that improve commute options for low-income workers.
    **Responsible Parties:** CCRPC Staff, IDOT, Cities and Villages, University of Illinois, Champaign County EDC, Champaign County Chamber of Commerce

7.	Emphasize transportation investments that provide and encourage active modes of transportation and increase travel options, particularly in designated centers, to meet the distinctive needs of the regional economy.
    **Responsible Parties:** CUUATS Staff, IDOT, Cities and Villages, University of Illinois, Champaign County EDC, Champaign County Chamber of Commerce

8.	Identify Economically Disconnected and Disinvested areas in the region while also considering transportation accessibility utilizing tools such as Access Score.
    **Responsible Parties:** CUUATS Staff

12.	Support transportation projects that increase access to training locations (i.e. Parkland, WEA).
    **Responsible Parties:** CUUATS Staff, IDOT, Cities and Villages, University of Illinois, Champaign County EDC, Champaign County Chamber of Commerce

13.	Continue to pursue federal and state grants for transportation infrastructure and services when and where appropriate.
    **Responsible Parties:** CUUATS Staff

15.	Ensure that funding is available to maintain or replace transportation assets on appropriate schedules to preserve the existing transportation system.
    **Responsible Parties:** IDOT, Cities, Villages, University of Illinois, CUUATS Staff

16.	Work with downtown business owners, employees, and residents regarding active transportation preferences to include in local bicycle and pedestrian plans.
    **Responsible Parties:** Cities of Champaign and Urbana

17.	Implement complete streets connecting to and between downtown districts.
    **Responsible Parties:** Cities of Champaign and Urbana

18.	Increase facilities for alternative vehicles in downtowns (e.g., electronic charging stations).
    **Responsible Parties:** Cities of Champaign and Urbana

19.	Increase the number and/or frequency of Amtrak routes.
    **Responsible Parties:** Amtrak

20.	Plan for more development and transportation access in the area surrounding the airport, with a focus on the land between the airport and I-57.
    **Responsible Parties:** Willard Airport, University of Illinois, Village of Savoy, CUUATS staff

21.	Identify cost effective ways to provide transit service to Willard Airport.
    **Responsible Parties:** Willard Airport, University of Illinois, Village of Savoy, MTD, CUUATS staff

22.	Support efforts by IDOT, the Midwest High Speed Rail Association, and other related entities to designate the Chicago-Champaign-St. Louis route as a federally studied and approved high speed rail corridor.
    **Responsible Parties:** High Speed Rail Association or IDOT

23.	Implement the infrastructure projects and non-infrastructure strategies proposed in the Champaign-Urbana Region Freight Plan.
    **Responsible Parties:** CUUATS Staff, IDOT, Cities and Villages, University of Illinois, Champaign County EDC, Champaign County Chamber of Commerce, Norfolk Southern Railway, Mid-West Truckers Association

24.	Maintain and enhance regional data-sharing through platforms such as the Champaign County Regional Data Portal.
    **Responsible Parties:** CCRPC Staff, IDOT, Cities and Villages, University of Illinois, MTD

25.	Deploy autonomous vehicle (AV) pilot projects to better connect people to jobs and job training.
    **Responsible Parties:** CCRPC Staff, IDOT, Cities and Villages, University of Illinois, Champaign County EDC, Champaign County Chamber of Commerce

26.	Favor policies and projects that encourage greater fuel efficiency and non-fossil-based fuels.
    **Responsible Parties:** CCRPC Staff, IDOT, Cities and Villages, University of Illinois, Champaign County EDC, Champaign County Chamber of Commerce

28.	Strengthen the coordination of local and regional planning for transportation and economic development. Use the MPO as a forum to coordinate regional planning and projects.
    **Responsible Parties:** CCRPC Staff, Cities and Villages, Developers

29.	Identify locations that are highly visible to visitors and potential investors where streetscaping could increase appeal (i.e. Lincoln Avenue between I-74 and the University of Illinois).
    **Responsible Parties:** City of Urbana, IDOT
