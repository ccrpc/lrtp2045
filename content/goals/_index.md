---
title: Goals
date: 2018-09-18T13:50:42-06:00
draft: false
menu: main
weight: 25
---

The five overarching long-term goals are safety, multimodal
connectivity, equity, economy, and environment. These goals and their
objectives are based on a combination of the Federal transportation goals, State
of Illinois transportation goals, local knowledge, current local planning
efforts, and input received from the public.
