---
title: Home
date: 2018-09-18T13:50:42-06:00
draft: false
bannerHeading: "LRTP 2045"
bannerText: >
  As a community transportation policy document, the LRTP provides a regional
  transportation vision for 2045 to guide future transportation
  investments.

---
***Welcome to the Champaign-Urbana Long Range Transportation Plan (LRTP) 2045!***  

The LRTP 2045 was approved in December 2019 and will be in effect from 2020
through 2024. As a web-based plan, all LRTP 2045 content is contained in this
public website rather than on paper. A web-based plan allows for more
interactive content with links to related resources, downloadable data, and
interactive maps. The content is organized in the following sections:
**Overview, Existing Conditions, Goals, 2045 Vision, Public Involvement, and
Appendices.** Explore the plan by clicking on the section drop-down tabs
at the top of the page which are also summarized in the menu below.
