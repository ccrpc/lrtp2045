---
title: Public Involvement
date: 2018-09-18T13:50:42-06:00
draft: false
menu: main
weight: 40
---

LRTP outreach includes educating the public about the long range transportation
planning process, raising awareness of existing transportation services, and
providing opportunities for the public to inform the direction of planning
efforts. Special emphasis is placed on public input because the transportation
system affects every resident, employee, and visitor in the community.
