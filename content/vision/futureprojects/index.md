---
title: "Future Projects"
date: 2018-09-18T13:50:42-06:00
draft: false
Weight: 30
bannerHeading: Future Projects
bannerText: >
  This chapter sets forth a vision for each mode of transportation in the
  urbanized area through the year 2040.
---

CUUATS staff used [public input from the first round of LRTP 2045
outreach](https://ccrpc.gitlab.io/lrtp2045/process/round-one/), input from local
agencies, and local planning documents to establish the LRTP 2045 goals (safety,
multimodal connectivity, economy, equity, and the environment) along with a list
of transportation system changes that could contribute to the realization of the
goals. Public input regarding the *current* transportation system called
attention to locations in the system that people thought were functioning well
and locations where people suggested specific infrastructure improvements.
Public input regarding the *future* transportation system came from responses to
a question asking people to prioritize the three most important factors that
will shape our transportation system over the next 25 years. The combined input
about the current and future transportation system conveyed strong public
support for a set of overlapping ideas about the future of transportation: a
more environmentally sustainable transportation system, additional pedestrian
and bicycle infrastructure, shorter off-campus transit times, equitable access
to transportation services, and a compact urban area that supports active
transportation and limits sprawl development.

## Future Projects: Regionally Significant Vision Projects ##

The graphic below highlights the overarching transportation goals for the future
alongside regionally-significant transportation projects as part of the LRTP
2045 future vision. The regionally-significant projects and collectively-defined
goals are connected in the graphic to illustrate how individual improvement
projects can complement other projects to improve the overall system over time.
Some projects are already funded and in progress while others are considered an
illustrative, un-funded part of the LRTP vision for 2045.

{{<image src="LRTP Vision Board_22x34_reduced.jpg"
  alt="Chart of LRTP 2045 goals listed with regionally significant vision projects and an illustrative map"
  caption="Note: To expand the image to full-size, right-click the graphic and choose 'open image in new tab'."
  attr="CUUATS" attrlink="https://ccrpc.org/"
  position="full">}}

  The intent of the following vision video is to get people thinking about the future and imagining different changes in the local community in 2045. This video is not intended to be a comprehensive or literal representation of the LRTP 2045.
  <iframe width="560" height="315" src="https://www.youtube.com/embed/MwJMs9c31dc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Future Projects: Fiscally Constrained ##

As part of the LRTP, the Federal Highway Administration requires a listing of
the fiscally constrained projects that are part of the overall vision for the
urbanized area. The fiscally constrained projects are those that have either
guaranteed or reasonably guaranteed funding secured for the completion of the
project. A separate federally-required document for the region, the
Transportation Improvement Program (TIP) lists fiscally-constrained
transportation projects anticipated to be constructed in the metropolitan
planning area during the next four years. The program reflects the goals,
objectives, and recommendations from the 25-year Long Range Transportation Plan
(LRTP) in the short term. By mandate, the TIP must include projects receiving
federal and state funding; the CUUATS TIP also includes local projects from our
member agencies. Compiling the document requires a consensus among all CUUATS
agencies, thus establishing compliance with federal laws and eligibility for
federal funding. Each year, the Illinois Department of Transportation (IDOT)
publishes a fiscally-constrained six-year program that details transportation
investments in the state and local highway system. As of September 2019, the
State of Illinois has not yet released the state’s multi-year transportation
improvement program due to the pending implementation of the Capital Bill
approved in July 2019. The local TIP will be amended to reflect additional
State-funded transportation projects as they are released. The following table
includes a summary of local projects reflected in the current TIP for fiscal
years 2020-2023. Visit the online [TIP map and
database](https://maps.ccrpc.org/tip-2020-2023/) for more details about these
projects and other transportation expenditures in the region.

<rpc-table url="TIP summary 20191008.csv"
  table-title="CUUATS TIP Summary"
  text-alignment="c,c"></rpc-table>

Furthermore, the City of Champaign, City of Urbana, Village of Savoy, and
Village of Mahomet identify additional infrastructure maintenance and
improvement projects in their respective Capital Improvement Plans and Programs.

## Future Projects: Local and Unfunded ##

Each agency in the region has its own set of transportation priorities and goals
to improve mobility in their respective jurisdiction alone and in conjunction
with surrounding jurisdictions. The following list includes local project
priorities for the future that are currently un-funded. The projects are
numbered in the map for identification purposes only and do not indicate
priority.

<iframe src="https://maps.ccrpc.org/lrtp2045-roadway-vision-projects/"
  width="100%" height="600" allowfullscreen="true">
  Map showing roadway vision projects.
</iframe>

<rpc-table url="localunfunded_numbered.csv"
  table-title="Future Projects: Local and Unfunded"
  text-alignment="c,c"></rpc-table>

Bicycling and walking projects were identified separately from roadway projects
due to their large volume and small scale. The recommendations from the
following plans were incorporated into the map below:

- 2017 Champaign Park District Trails Master Plan (CTMP) – approved June 2017
- 2017 Savoy Bike & Pedestrian Plan – approved April 2017
- 2016 Urbana Bicycle Master Plan (UBMP) – approved December 2016
- 2016 Urbana Park District Trails Master Plan (UTMP) – approved January 2016
- 2014 Champaign County Greenways & Trails Plan – approved June 2014

<iframe src="https://maps.ccrpc.org/lrtp2045-bicycle-pedestrian-vision/"
  width="100%" height="600" allowfullscreen="true">
  Map of the future pedestrian, bike, and trail projects included in 2017 Champaign Park District Trails Master Plan (CTMP), 2017 Savoy Bike & Pedestrian Plan, 2016 Urbana Bicycle Master Plan (UBMP), 2016 Urbana Park District Trails Master Plan (UTMP), and 2014 Champaign County Greenways & Trails Plan.
</iframe>

The implementation of the local projects described in the following tables are
not the responsibility of CUUATS or the Champaign County Regional Planning
Commission. These projects have been included in the LRTP to highlight local
improvement projects that will enhance the quality of the overall transportation
network and will help connect the local transportation network to the planned
regionally significant improvements for the future.
