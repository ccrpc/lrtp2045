---
title: "Goals"
date: 2018-09-18T13:50:42-06:00
draft: true
Weight: 20
bannerHeading: Goals, Objectives & Performance Measures
bannerText: >
  The formulation of goals and objectives determines what direction planning
  efforts should take.
---

## Overview

The  Champaign Urbana Urbanized Area Transportation Study (CUUATS) staff, in
conjunction with the Long Range Transportation Plan (LRTP) Steering Committee,
developed goals with related objectives and performance measures to reflect the
long range transportation vision for 2045 and provide direction for the plan’s
implementation. The five overarching long-term goals are **safety, multimodal
connectivity, equity, economy, and environment**. These goals and their
objectives are based on a combination of the Federal transportation goals, State
of Illinois transportation goals, local knowledge, current local planning
efforts, and input received from the public. Additionally, short-term objectives
and performance measures translate the long range vision for 2045 into short
term action. The CUUATS staff uses the SMART (specific, measurable, agreed,
realistic, and timebound) acronym to guide the objective development process.

•	***Goals*** are defined as an end state that will be brought about by implementing
the plan.

•	***Objectives*** are sub-goals that help organize the plan implementation into
manageable parts.

•	***Performance Measures*** are metrics used to help agencies track the progress toward
each objective over time.

•	***Strategies*** are ideas or guiding principles to help agencies reach the stated goals and objectives.

## Measuring progress

Each year CUUATS staff updates the Annual LRTP Report Card to measure and
document the progress made toward the current LRTP’s short-term objectives using
the performance measures. The Annual LRTP Report Card helps provide transparency
and continuity between five-year LRTP updates by identifying opportunities and
barriers to achieving LRTP goals and objectives. Each objective’s performance
measure (or measures) are assigned a good, neutral, or negative rating depending
on the circumstances and data trend of each measure. The Report Card is reviewed
by the CUUATS Technical Policy Committees each year and is available online for
local agencies and members of the public.

The Annual LRTP 2045 Report Card will track the performance measures in the LRTP
2045 for the active years of the plan, 2020 to 2024, with 2020 as the base year.
The [current Annual LRTP 2040 Report Card](https://reportcard.cuuats.org/)
tracks the performance measures in the LRTP 2040 for the years 2015 through
2019, with 2015 as the base year.

In addition to helping the metropolitan planning area refine its vision, the
LRTP 2045 has been updated in accordance with federal requirements. In May 2016,
the Federal Highway Administration (FHWA) and Federal Transit Administration
(FTA) jointly issued the final rule on Statewide and Nonmetropolitan
Transportation Planning and Metropolitan Transportation Planning, implementing
changes to the planning processes that had been established by the Moving Ahead
for Progress in the 21st Century Act (MAP21) and the Fixing America’s Surface
Transportation Act (FAST Act). These changes established additional performance
measures intended to ensure effective investment of federal transportation
funds. The federal measures include five safety measures which are reflected in
the LRTP 2045 safety measures listed above, as well as six infrastructure, 3
system performance, and 9 transit asset conditions measures. These additional
measures are currently reflected in CUUATS Annual Transportation Improvement
Program (TIP) documentation and will be monitored every two years with TIP
updates, separately from the LRTP 2045 Annual Report Card.

{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title="National Transportation Goals"%}}
  **Safety:** To achieve a significant reduction in traffic fatalities and serious injuries on all public roads.    

  **Infrastructure Condition:** To maintain the highway infrastructure asset system in a state of good repair.

  **Congestion Reduction:** To achieve a significant reduction in congestion on the National Highway System.

  **System Reliability:** To improve the efficiency of the surface transportation system.

  **Freight Movement and Economic Vitality:** To improve the National Highway Freight Network, strengthen the ability of rural communities to access national and international trade markets, and support regional economic development.

  **Environmental Sustainability:** To enhance the performance of the transportation system while protecting and enhancing the natural environment.

  **Reduced Project Delivery Delays:** To reduce project costs, promote jobs and the economy, and expedite the movement of people and goods by accelerating project completion through eliminating delays in the project development and delivery process, including reducing regulatory burdens and improving agencies' work practices.

  *Source: United States Code, 23 U.S.C. 150(b)*
  {{%/accordion-content%}}
  {{%accordion-content title="State Transportation Goals"%}}
  **Economy:** Improve Illinois’ economy by providing transportation infrastructure that supports the efficient movement of people and goods.

  **Access:** Support all modes of transportation to improve accessibility and safety by improving connections between all modes of transportation.

  **Livability:** Enhance quality of life across the state by ensuring that transportation investments advance local goals, provide multimodal options and preserve the environment.

  **Stewardship:** Safeguard existing funding the increase revenues to support system maintenance, modernization and strategic growth of Illinois’ transportation system.

  **Resilience:** Proactively assess, plan, and invest in the state’s transportation system to ensure that our infrastructure is prepared to sustain and recover from extreme weather events and other disruptions.

  *Source: Illinois Department of Transportation, Long Range Transportation Plan, 2019*
  {{%/accordion-content%}}
{{</accordion>}}
{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title="LRTP 2045 Goals"%}}
  **Safety:** The metropolitan planning area will maintain, preserve, and operate its
  existing transportation system to provide safe, efficient, and reliable movement
  of people, goods, and services in the short term, and in the long term, achieve
  the state’s goal of zero death and disabling injuries.    

  **Multimodal Connectivity:** The Champaign-Urbana area will increase accessibility, connectivity, and
  mobility of people and freight to all areas of the region by maintaining a
  multimodal system of transportation that is cost-effective for people,
  businesses, and institutions and will allow freedom of choice in all modes of
  transportation including active modes whenever possible.

  **Equity:** The metropolitan planning area will aim to provide safe, affordable, and efficient transportation choices to all people to support a high quality of life in every part of the community.

  **Economy:**  The metropolitan planning area will maximize the efficiency of the local transportation network for area employers, employees, materials, products, and clients at the local, regional, national, and global levels and facilitate strong inter-regional collaborations to realize large-scale transportation improvements.

  **Environment:** The metropolitan planning area will reduce the transportation system’s significant contribution to greenhouse gas emissions and environmental degradation to maintain a high quality of human and environmental health in the region.

  {{%/accordion-content%}}
{{</accordion>}}

## LRTP Goal: Safety

**Goal:** The metropolitan planning area will maintain, preserve, and operate its
existing transportation system to provide safe, efficient, and reliable movement
of people, goods, and services in the short term, and in the long term, achieve
the state’s goal of zero death and disabling injuries.

<rpc-table url="safety_table.csv"
  table-title="Safety Objectives and Performance Measures"
  text-alignment="c,c"></rpc-table>

{{<accordion border="true" multiselect="false" level="3">}}
  {{%accordion-content title="Safety Strategies and Responsible Parties"%}}
1.	Implement the strategies approved in the Champaign-Urbana Urban Area Safety Plan
for safety emphasis areas, including intersections, pedestrians, bicyclists, and
impaired driving.
**Responsible Parties:** CUUATS staff, IDOT, Champaign County, local cities, villages and townships (Public Works), MTD, CU SRTS Project, University of Illinois, local and state law enforcement agencies, local EMS and hospitals, judiciary system

2.	Continue to facilitate regional Safety Committee to support collaboration between planners, engineers, law enforcement, and other community partners.
**Responsible Parties:** CUUATS staff, IDOT, Champaign County, local cities, villages and townships (Public Works), MTD, CU SRTS Project, University of Illinois, local and state law enforcement agencies, local EMS and hospitals, judiciary system

3.	Promote safety in the planning, design, construction, and maintenance of all modes in transportation projects and programs (e.g., designing for the incorporation of emerging safety-related technologies).
**Responsible Parties:** CUUATS, IDOT, Cities, Villages, MTD, University of Illinois

4.	Prepare applications and provide input to local agencies regarding Highway Safety Improvement Program (HSIP) funds.
**Responsible Parties:** CUUATS staff

5.	Complete applications for available Federal safety funding.
**Responsible Parties:** CUUATS staff

6.	Evaluate HSIP projects by completing before and after studies.
**Responsible Parties:** IDOT

7.	Continue to enforce codes requiring new development to provide sidewalks along roadway frontages and safe crossings at intersections.
**Responsible Parties:** CUUATS Staff, Cities and Villages, Developers, University of Illinois

9.	Continue to provide and upgrade sidewalk infrastructure according to ADA standards.
**Responsible Parties:** CUUATS Staff, Cities and Villages, University of Illinois

10.	Revise, complete and distribute Safe Walking Route Maps for public elementary and middle schools in Champaign-Urbana every two years and continue the Safe Routes to School program.
**Responsible Parties:** CUUATS staff, C-U SRTS Project

11.	Work with municipalities and transportation study groups to evaluate existing speed limits on the local roadway network.
**Responsible Parties:** CUUATS staff

12.	Perform Road Safety Audit (RSA) at the request of local agencies, maintain list of trained volunteers to help conduct RSAs.
**Responsible Parties:** CUUATS staff

13.	Assess the regional transportation network for safe routing of hazardous materials and designate the most appropriate routes for hazmat transportation.
**Responsible Parties:** CUUATS staff, Cities and Villages, Champaign County Emergency Management Agency (EMA), Developers, Champaign County LEPC, law enforcement, CUMTD, University of Illinois

14.	Finish updating the regional Intelligent Transportation System (ITS) architecture and install Vehicle Management Systems (VMS) at major roadways and intersections when appropriate.
**Responsible Parties:** CUUATS staff, Cities and Villages, Champaign County Emergency Management Agency (EMA), Developers, Champaign County LEPC, law enforcement, CUMTD, University of Illinois

15.	Perform periodic emergency evacuation drills at different agencies including local school districts.
**Responsible Parties:** CUUATS staff, Cities and Villages, Champaign County Emergency Management Agency (EMA), Developers, Champaign County LEPC, law enforcement, CUMTD, University of Illinois

16.	Create an evacuation plan for the region that would set the regional transportation system to be ready for efficiently performing evacuation in case of a natural or man-made disaster.
**Responsible Parties:** CUUATS staff, Cities and Villages, Champaign County EMA, LEPC, school districts, law enforcement, MTD

17.	Coordinate with IDOT, Department of Homeland Security (DHS), and local agencies to ensure that up to date security features are installed at relevant regional transportation infrastructure.
**Responsible Parties:** CUUATS staff, DHS, IDOT, law enforcement, Cities and Villages, CUMTD, University of Illinois

18.	Ensure robust and meaningful community engagement for communities to identify and develop solutions to the transition to a future with automated vehicles (AVs). This includes community engagement in developing AV regulations.
**Responsible Parties:** IDOT, CUUATS, Cities, Villages, University of Illinois, MTD

  {{%/accordion-content%}}
{{</accordion>}}

## LRTP Goal: Multimodal Connectivity

**Goal:** The Champaign-Urbana area will increase accessibility, connectivity, and
mobility of people and freight to all areas of the region by maintaining a
multimodal system of transportation that is cost-effective for people,
businesses, and institutions and will allow freedom of choice in all modes of
transportation including active modes whenever possible.

<rpc-table url="mutlimodal_table.csv"
  table-title="Multimodal Connectivity Objectives and Performance Measures"
  text-alignment="c,c"></rpc-table>


  {{<accordion border="true" multiselect="false" level="3">}}
    {{%accordion-content title="Multimodal Connectivity Strategies and Responsible Parties"%}}
1.	Continue to implement proposed improvements to bicycle infrastructure approved in regional and local bicycle, and greenways and trails plans.
**Responsible Parties:** CUUATS Staff, Cities and Villages, Developers, University of Illinois

2.	Continue to enforce codes requiring new development to provide sidewalks along roadway frontages and safe crossings at intersections.
**Responsible Parties:** CUUATS Staff, Cities and Villages, Developers, University of Illinois

3.	Continue to provide and upgrade sidewalk infrastructure according to ADA standards.
**Responsible Parties:** CUUATS Staff, Cities and Villages, University of Illinois

4.	Implement strategies proposed in the Sidewalk Network Inventory and Assessment for the Champaign-Urbana Urbanized Area, including recommendations under the categories of Compliance, Condition, Connectivity, Priority Areas and Funding.
**Responsible Parties:** Cities, Villages, and University of Illinois

5.	Revise, complete and distribute Safe Walking Route Maps for public elementary and middle schools in Champaign-Urbana every two years and continue the Safe Routes to School program.
**Responsible Parties:** CUUATS staff, C-U SRTS Project

6.	Identify cost effective ways of including bicycle, pedestrian, and transit accommodations into all new roadway projects.
**Responsible Parties:** IDOT, Cities, Villages, University of Illinois, Developers, Townships

7.	Consult with existing bicycle, pedestrian, and greenways and trails plans and local agencies to coordinate infrastructure priorities.
**Responsible Parties:** IDOT, Cities, Villages, and University of Illinois

8.	Take advantage of opportunities to develop off-street shared-use paths, using methods including but not limited to: working with railroads to develop bicycle facilities on or along rights-of-way, and acquiring property that provides off-street connections between bicycle facilities.
**Responsible Parties:** Cities, Villages, University of Illinois, Park Districts of Champaign and Urbana, Champaign County Forest Preserve District, Developers

9.	Explore private/ public partnerships to implement multimodal infrastructure improvements in specific locations that benefit surrounding businesses.
**Responsible Parties:** Cities, Villages, University of Illinois, IDOT

10.	Coordinate with local law enforcement regarding new pedestrian and bicycle plans and associated education and enforcement activities, especially targeting motorists.
**Responsible Parties:** Police, Municipalities, University of Illinois, IDOT

11.	Develop stronger active transportation connections between downtown Champaign and downtown Urbana as recommended in IDOT’s 2015 University Avenue Road Safety Assessment and the 2010 University Avenue Corridor Study.
**Responsible Parties:** IDOT, Cities and University of Illinois

12.	Create active transportation connections between Savoy, the University Research Park, and the University of Illinois campus.
**Responsible Parties:** Village of Savoy, University of Illinois

13.	Connect underserved rural transit areas by linking rural transit services to urban transit service routes at connecting points.
**Responsible Parties:** MTD, CCARTS

14.	Annex additional urbanized area land into the MTD service area.
**Responsible Parties:** MTD

15.	Provide transit service to areas of new residential, commercial and/or industrial development.
**Responsible Parties:** MTD

16.	Evaluate existing routes and service times to determine if transit service is meeting resident and worker needs.
**Responsible Parties:** MTD

17.	Coordinate with Amtrak to provide more consistent fare pricing.
**Responsible Parties:** Amtrak

18.	Create at least 2 new regional or national flight connections that match nearby airport destinations or are unique destinations to the region to increase the appeal of Willard Airport to travelers.
**Responsible Parties:** Willard Airport, University of Illinois, MTD

19.	Identify cost effective ways to provide transit service to Willard Airport.
**Responsible Parties:** Willard Airport, University of Illinois, MTD

20.	Identify opportunities to install wayfinding signage for all modes of transportation.
**Responsible Parties:** CUUATS member agencies, Visit Champaign County

21.	Support efforts by IDOT, the Midwest High Speed Rail Association, and other related entities to advance the case for a Chicago-Champaign-St. Louis high speed rail corridor.
**Responsible Parties:** High speed rail consortium or IDOT

22.	Use community-wide calendars to promote multimodal transportation to existing events.
**Responsible Parties:** CUUATS Staff

23.	Continue to market the benefits of lifestyles free of car ownership to existing and future students and residents.
**Responsible Parties:** CUMTD, Cities, Villages, and University of Illinois

24.	Set up information tables for transportation-related public outreach at popular events listed on municipal calendars of public events (i.e. Neighborhood Nights, Sounds at Sunset, Orchard Days, RC Fest, sporting events, etc.).
**Responsible Parties:** CUUATS member agencies and all local municipalities

25.	Distribute at least 1 type of educational and/or encouragement material related to transportation modes, facilities, and benefits to K-12 schools.
**Responsible Parties:** CUUATS member agencies and all local municipalities

26.	Adopt CUUATS Access Management Guidelines into municipal codes or ordinances.
**Responsible Parties:** Cities, Villages, University of Illinois

27.	Adhere to the CUUATS Complete Streets Policy for all new and reconstruction transportation infrastructure projects
**Responsible Parties:** Cities, Villages, University of Illinois

28.	Apply for Illinois Transportation Enhancement Program (ITEP) grants when feasible to fund pedestrian and bicycle infrastructure projects.
**Responsible Parties:** Cities and Villages Public Works Departments

29.	Plan additional industrial subdivisions to efficiently connect with existing transportation infrastructure (e.g., sidewalks, bike paths, transit, roads, freight routes, etc.).
**Responsible Parties:** Cities, Villages, IDOT, CUUATS Staff

30.	Improve region-wide data on bicycle and pedestrian infrastructure usage and travel patterns.
**Responsible Parties:** CUUATS Staff, Cities, and Villages

31.	Acquire and analyze regional bike-share data.
**Responsible Parties:** CUUATS Staff, Cities, Villages, Private Sector

32.	Help local agencies and the public to identify the potential benefits and pitfalls of new mobility technologies, including autonomous and connected vehicles, with regard to safety, economic competitiveness, affordable mobility, accessibility, and environmental impacts.
**Responsible Parties:** IDOT, CUUATS Staff

33.	Convene and coordinate regional stakeholders to engage in national, state, and regional-level conversations about automated and connected vehicle policy and industry standards.
**Responsible Parties:** IDOT, CUUATS Staff, MTD

34.	Establish localized policies for automated vehicles (AVs), Transportation Network Companies (TNCs), and other emerging technologies that support multimodal connectivity goals.
**Responsible Parties:** CUUATS Staff, IDOT, Cities, Villages, University of Illinois, MTD

35.	Pilot new approaches for integrating automated vehicles (AVs) with strategies established to support and complement public transit and preserve safe, vibrant, equitable, accessible, and walkable communities. 	 
**Responsible Parties:** CUUATS Staff, IDOT, Cities, Villages, University of Illinois, MTD

36.	Coordinate with other public agencies and partners in academia to develop analytical tools and track the impact of emerging technologies. 	
**Responsible Parties:** CUUATS Staff, IDOT

38.	Encourage schools to work with municipal departments to implement engineering and enforcement recommendations proposed in SRTS plans.
**Responsible Parties:** Champaign School District, Urbana School District, Cities of Champaign and Urbana, C-U SRTS Project, CUUATS

39.	Work with local agencies to define encouragement and enforcement measures for snow removal.
**Responsible Parties:** Local municipalities and Public Works Departments

40.	Create new bike encouragement events like, Bike-N-Dine, Polar Bear Bike Ride, Smart Trip Planning, or Spring Into Action Walk About.
    **Responsible Parties:** CUUATS, C-U SRTS Project, local bike groups, all local municipalities, local Park Districts

41.	Produce and distribute a regularly updated map including bike, trail, and park facilities.
    **Responsible Parties:** CUUATS, C-U SRTS Project, local bike groups, all local municipalities, local Park Districts

    {{%/accordion-content%}}
  {{</accordion>}}

## LRTP Goal: Equity

**Goal:** The metropolitan planning area will aim to provide safe, affordable, and
efficient transportation choices to all people to support a high quality of life
in every part of the community.

<rpc-table url="equity_table.csv"
  table-title="Equity Objectives and Performance Measures"
  text-alignment="c,c"></rpc-table>

  {{<accordion border="true" multiselect="false" level="3">}}
    {{%accordion-content title="Equity Strategies and Responsible Parties"%}}
1.	Use Access Score to identify specific strategies to improve mobility and accessibility in new transportation planning and development efforts.
    **Responsible Parties:** CUUATS Staff, Cities and Villages, University of Illinois

2.	Implement strategies proposed in the Sidewalk Network Inventory and Assessment for the Champaign-Urbana Urbanized Area, including recommendations under the categories of Compliance, Condition, Connectivity, and Priority Areas and Funding.
    **Responsible Parties:** Cities, Villages, and University of Illinois

3.	Install new bike self-repair stations in areas that don't already have any.
    **Responsible Parties:** Cities, Villages, and University of Illinois, Park Districts, Champaign County Forest Preserve District

4.	Investigate what percentage of annual transit pass cost would be cost-effective to price an affordable, annual transit pass for high school-aged youth.
    **Responsible Parties:** MTD

5.	Investigate the feasibility of allowing middle and high-school aged students to use their school IDs to ride the bus for free at any time.
    **Responsible Parties:** MTD

6.	Evaluate existing routes and service times to determine if transit service is meeting resident and/or worker demands, particularly in low-income areas, and identify areas for expansion of service where needed.
    **Responsible Parties:** MTD

7.	Continue to implement recommendations listed in the Transit Facility Guidelines.
    **Responsible Parties:** MTD

8.	Encourage Amtrak to create lower and more consistent fares.
    **Responsible Parties:** Amtrak

9.	Increase Amtrak service frequencies.
    **Responsible Parties:** Amtrak

10.	Utilize data obtained from Access Score to inform future development locations and recommendations.
    **Responsible Parties:** CUUATS Staff, Cities and Villages

11.	Develop a regional transportation directory.
    **Responsible Parties:** CUUATS Staff, Cities and Villages

12.	Identify Economically Disconnected and Disinvested areas in the region while also considering transportation accessibility utilizing tools such as Access Score.
    **Responsible Parties:** CUUATS Staff

13.	Promote strategic investment in disinvested areas.
    **Responsible Parties:** Champaign County Chamber of Commerce, Champaign County EDC, Cities, and Villages, CCRPC

14.	Work with financial institutions to apply for low cost loans for infrastructure projects that qualify under the Community Reinvestment Act (CRA).
    **Responsible Parties:** Cities and Villages

15.	Support projects that improve commute options for low-income workers.
    **Responsible Parties:** CCRPC Staff, IDOT, Cities and Villages, University of Illinois, Champaign County EDC, Champaign County Chamber of Commerce

16.	Continue to pursue federal and state grant and other funding opportunities when and where appropriate.
    **Responsible Parties:** CUUATS Staff

17.	Develop guidance to ensure that any partnerships with private mobility services provide clear public benefits, are accessible to people with disabilities, and include protections for low income communities against sudden changes in the private market.
    **Responsible Parties:** CCRPC and MTD

18.	Hold project open houses when appropriate and make project materials available for the public.
    **Responsible Parties:** CUUATS member agencies

19.	Organize project advisory committees and invite all relevant stakeholders to sit at the table.
    **Responsible Parties:** CUUATS member agencies

20.	Use emerging technologies such as autonomous vehicles to build capacity for transit service and advance equity.
    **Responsible Parties:** CUUATS, MTD

21.	Ensure that emerging transportation technologies like autonomous vehicles are rolled out in a way that prevents displacement or disruption of active transportation.
    **Responsible Parties:** IDOT, CUUATS, Cities, Villages, University of Illinois, MTD

22.	Work to prevent any new autonomous vehicle infrastructure from segregating neighborhoods.
    **Responsible Parties:** IDOT, CUUATS, Cities, Villages, University of Illinois, MTD

23.	Ensure robust and meaningful community engagement for communities to identify and develop solutions to the transition to an AV future. This includes community engagement in developing AV regulations.
    **Responsible Parties:** IDOT, CUUATS, Cities, Villages, University of Illinois, MTD

24.	Update and maintain the Health Impact Assessment for the region with local health data to inform transportation investment priorities and support active transportation and physical activity in locations with high obesity and related disease rates.
    **Responsible Parties:** CUUATS, CUPHD, Healthcare providers

    {{%/accordion-content%}}
  {{</accordion>}}

## LRTP Goal: Economy

**Goal:** The metropolitan planning area will maximize the efficiency of the local
transportation network for area employers, employees, materials, products, and
clients at the local, regional, national, and global levels and facilitate
strong inter-regional collaborations to realize large-scale transportation
improvements.

<rpc-table url="economy_table.csv"
  table-title="Economy Objectives and Performance Measures"
  text-alignment="c,c"></rpc-table>

  {{<accordion border="true" multiselect="false" level="3">}}
    {{%accordion-content title="Economy Strategies and Responsible Parties"%}}
1.	Favor transportation policies and projects that result in greater job creation.
    **Responsible Parties:** CCRPC Staff, IDOT, Cities and Villages, University of Illinois, Champaign County EDC, Champaign County Chamber of Commerce

2.	Integrate transportation and land use planning to maximize the supply of development that can occur in accessible, multi-modal areas, in conjunction with pricing reforms that favor accessible locations.
    **Responsible Parties:** CCRPC Staff, Cities and Villages, Developers

4.	Plan additional industrial subdivisions to efficiently connect with existing transportation infrastructure (e.g., sidewalks, bike paths, transit, roads, freight routes, etc.).
    **Responsible Parties:** Cities, Villages, IDOT, CUUATS Staff

5.	Improve key transportation facilities and services connecting the region to national and international markets.
    **Responsible Parties:** CUUATS Staff, IDOT, Cities and Villages, University of Illinois, Champaign County EDC, Champaign County Chamber of Commerce

6.	Support projects that improve commute options for low-income workers.
    **Responsible Parties:** CCRPC Staff, IDOT, Cities and Villages, University of Illinois, Champaign County EDC, Champaign County Chamber of Commerce

7.	Emphasize transportation investments that provide and encourage active modes of transportation and increase travel options, particularly in designated centers, to meet the distinctive needs of the regional economy.
    **Responsible Parties:** CUUATS Staff, IDOT, Cities and Villages, University of Illinois, Champaign County EDC, Champaign County Chamber of Commerce

8.	Identify Economically Disconnected and Disinvested areas in the region while also considering transportation accessibility utilizing tools such as Access Score.
    **Responsible Parties:** CUUATS Staff

12.	Support transportation projects that increase access to training locations (i.e. Parkland, WEA).
    **Responsible Parties:** CUUATS Staff, IDOT, Cities and Villages, University of Illinois, Champaign County EDC, Champaign County Chamber of Commerce

13.	Continue to pursue federal and state grants for transportation infrastructure and services when and where appropriate.
    **Responsible Parties:** CUUATS Staff

15.	Ensure that funding is available to maintain or replace transportation assets on appropriate schedules to preserve the existing transportation system.
    **Responsible Parties:** IDOT, Cities, Villages, University of Illinois, CUUATS Staff

16.	Work with downtown business owners, employees, and residents regarding active transportation preferences to include in local bicycle and pedestrian plans.
    **Responsible Parties:** Cities of Champaign and Urbana

17.	Implement complete streets connecting to and between downtown districts.
    **Responsible Parties:** Cities of Champaign and Urbana

18.	Increase facilities for alternative vehicles in downtowns (e.g., electronic charging stations).
    **Responsible Parties:** Cities of Champaign and Urbana

19.	Increase the number and/or frequency of Amtrak routes.
    **Responsible Parties:** Amtrak

20.	Plan for more development and transportation access in the area surrounding the airport, with a focus on the land between the airport and I-57.
    **Responsible Parties:** Willard Airport, University of Illinois, Village of Savoy, CUUATS staff

21.	Identify cost effective ways to provide transit service to Willard Airport.
    **Responsible Parties:** Willard Airport, University of Illinois, Village of Savoy, MTD, CUUATS staff

22.	Support efforts by IDOT, the Midwest High Speed Rail Association, and other related entities to designate the Chicago-Champaign-St. Louis route as a federally studied and approved high speed rail corridor.
    **Responsible Parties:** High Speed Rail Association or IDOT

23.	Implement the infrastructure projects and non-infrastructure strategies proposed in the Champaign-Urbana Region Freight Plan.
    **Responsible Parties:** CUUATS Staff, IDOT, Cities and Villages, University of Illinois, Champaign County EDC, Champaign County Chamber of Commerce, Norfolk Southern Railway, Mid-West Truckers Association

24.	Maintain and enhance regional data-sharing through platforms such as the Champaign County Regional Data Portal.
    **Responsible Parties:** CCRPC Staff, IDOT, Cities and Villages, University of Illinois, MTD

25.	Deploy autonomous vehicle (AV) pilot projects to better connect people to jobs and job training.
    **Responsible Parties:** CCRPC Staff, IDOT, Cities and Villages, University of Illinois, Champaign County EDC, Champaign County Chamber of Commerce

26.	Favor policies and projects that encourage greater fuel efficiency and non-fossil-based fuels.
    **Responsible Parties:** CCRPC Staff, IDOT, Cities and Villages, University of Illinois, Champaign County EDC, Champaign County Chamber of Commerce

28.	Strengthen the coordination of local and regional planning for transportation and economic development. Use the MPO as a forum to coordinate regional planning and projects.
    **Responsible Parties:** CCRPC Staff, Cities and Villages, Developers

29.	Identify locations that are highly visible to visitors and potential investors where streetscaping could increase appeal (i.e. Lincoln Avenue between I-74 and the University of Illinois).
    **Responsible Parties:** City of Urbana, IDOT


    {{%/accordion-content%}}
  {{</accordion>}}

## LRTP Goal: Environment

**Goal:** The metropolitan planning area will reduce the transportation system’s
significant contribution to greenhouse gas emissions and environmental
degradation to maintain a high quality of human and environmental health in the
region.

<rpc-table url="environment_table.csv"
  table-title="Economy Objectives and Performance Measures"
  text-alignment="c,c"></rpc-table>

  {{<accordion border="true" multiselect="false" level="3">}}
    {{%accordion-content title="Environment Strategies and Responsible Parties"%}}
1.	Finish developing the Champaign County Ecological Framework and maintain the regional ecological database to aid in identifying and mitigating negative environmental impacts of transportation projects.
    **Responsible Parties:** CUUATS staff

2.	Develop a regional land use map and identify approaches to maintaining and updating the map.
    **Responsible Parties:** CCRPC/CUUATS staff

3.	Quantify the agricultural system’s contribution to the regional and local economies to better inform local economic development strategies, land use planning, and transportation investments.
    **Responsible Parties:** CUUATS, Champaign County Farm Bureau, Champaign County EDC, Champaign County Chamber of Commerce

4.	Encourage compact development practices.
    **Responsible Parties:** Cities, Villages

5.	Install alternative fueling and charging stations in areas of the community that don't already have them.
    **Responsible Parties:** Local municipalities and Public Works Departments

6.	Plan for integration of solar and charging stations into new roadway and development projects.
    **Responsible Parties:** Cities, Villages, University of Illinois, CUUATS staff

8.	Continue to implement recommendations listed in the Transit Facility Guidelines for the Champaign-Urbana Urbanized Area.
    **Responsible Parties:** MTD

9.	Continue to implement proposed improvements to bicycle infrastructure approved in regional and local bicycle, and greenways and trails plans.
    **Responsible Parties:** CUUATS Staff, Cities and Villages, University of Illinois

10.	Increase accessibility to transit services by providing missing sidewalk connections.
    **Responsible Parties:** Cities and Villages Public Works Departments, University of Illinois, IDOT

11.	Continue to provide and upgrade sidewalk infrastructure according to ADA standards.
    **Responsible Parties:** CUUATS Staff, Cities and Villages, University of Illinois

12.	Continue annual sidewalk inventory and assessment.
    **Responsible Parties:** CUUATS Staff, Cities, Villages, University of Illinois

13.	Promote active modes of transportation through various forms of encouragement (online materials, educational events, signage, etc.)
    **Responsible Parties:** Cities, Villages, University of Illinois, MTD, CUUATS

14.	Create and publicize community-wide calendar of events including Bike To Work Day, Walk n’ Roll to School Day, the Christie Clinic Illinois Marathon, and more.
    **Responsible Parties:** CUUATS, C-U SRTS Project, local bike groups, all local municipalities, local Park Districts, Champaign County Forest Preserve District

15.	Create new bike encouragement events like, Bike-N-Dine, Polar Bear Bike Ride, Smart Trip Planning, or Spring Into Action Walk About.
    **Responsible Parties:** CUUATS, C-U SRTS Project, local bike groups, all local municipalities, local Park Districts

16.	Produce and distribute a regularly updated map including bike, trail, and park facilities.
    **Responsible Parties:** CUUATS, C-U SRTS Project, local bike groups, all local municipalities, local Park Districts

17.	Utilize data obtained from Access Score to inform future development locations and recommendations.
    **Responsible Parties:** Cities, Villages

18.	Provide Complete Street connections to and between downtowns.
    **Responsible Parties:** Cities of Champaign and Urbana, CUUATS Staff

19.	Consider and avoid negative impacts of new and existing transportation projects on historically significant buildings, landmarks, districts, and/or structures.
    **Responsible Parties:** Cities, Villages, IDOT, CCRPC Staff

21.	Adopt conservation-oriented development standards that avoid development of key natural areas and ensure long-term stewardship of natural areas and open space.
    **Responsible Parties:** Cities and Villages

22.	Consider existing road, water, and wastewater infrastructure capacity in decisions about the intensity and extent of new development.
    **Responsible Parties:** Cities and Villages

23.	Reduce or eliminate minimum parking requirements, or set maximum parking limitations in some locations, such as near transit.
    **Responsible Parties:** Cities and Villages

24.	Incorporate green infrastructure and other green strategies into neighborhood parks, school yards and properties, corporate and office campuses, and other open lands.
    **Responsible Parties:** Cities, Villages,  Park Districts, School Districts, private sector, developers

25.	Identify an approach to incorporate climate resilience and adaptation measures into transportation projects and planning.
    **Responsible Parties:** Cities, Villages, University of Illinois, Champaign County Planning and Zoning, CCRPC

26.	Determine the vulnerability of transportation infrastructure to climate change impacts and design transportation infrastructure to withstand and adapt to the climate of its intended lifespan.
    **Responsible Parties:** IDOT, CUUATS Staff, Cities, Villages, University of Illinois, MTD

27.	Conduct an analysis of road performance under severe weather conditions to develop planned responses.
    **Responsible Parties:** IDOT, CUUATS Staff, Cities, Villages, University of Illinois, MTD

28.	Develop a regional pavement flooding reporting system to help plan for flood events.
    **Responsible Parties:** IDOT, CUUATS Staff, Cities, Villages, University of Illinois, MTD

29.	Encourage zero emission automated vehicles (AVs) with clean energy sources, to reduce air quality impacts.
    **Responsible Parties:** IDOT, CUUATS, Cities, Villages, University of Illinois, MTD

30.	Minimize potential sprawl induced by autonomous vehicles (AVs).
    **Responsible Parties:** IDOT, CUUATS, Cities, Villages, University of Illinois, MTD

31. conduct a study to identify and implement best practices to avoid the impacts of road salt as non-point source pollution.
    **Responsible Parties:** CUUATS, Cities, Villages


    {{%/accordion-content%}}
  {{</accordion>}}
