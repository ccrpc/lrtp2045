---
title: "Overview"
date: 2018-01-30T14:38:14-06:00
draft: false
menu: main
Weight: 10

---

This section contains an executive summary of the LRTP 2045 including two videos
illustrating the planning process and future vision. This section also provides
a history of local transportation infrastructure, local long range transportation
plans, and federal transportation planning legislation.
